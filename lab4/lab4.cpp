// Lab 4
// Description: Practice creating, manipulating linked lists, BGUnix.
//				A new DVD store in your neighborhood is about to pen. However, it does not have a program to keep track of its DVDs.
//				The program will be able to perform the following operations by asking the customers' choice:
//				1. Rent a DVD; that is, check out a DVD 
//				2. Return, or check in a DVD 
//				3. Display a list of DVDs owned by the store
//				4. Show the details of a particular DVD 
//				5. Check whether a particular DVD is in the store 
// Programmer:  Phuong Hai Nguyen
// Class:		CS 2020
// Semester:	Spring 2016

#include<iostream>
#include<string>
#include<iomanip>
#include<fstream>

using namespace std;

// Define a Node structure that contains: Name of the movie, name of the director, movie rating, number of copies owned by the store.
// Then declare a Next pointer in structure.
struct Node
{
	string movie, director, rate;
	int copies;
	Node *pNext;
};

// Function prototypes.
void CreateDVDLinkedList(Node* &);
void ShowDVDInformation(Node*);
void DisplayDVDLibrary(Node*);
Node* FindDVDbyTitle(Node*, string);
void CheckOut(Node*);
void CheckIn(Node*);
void DeleteList(Node*);

// Main Function.
int main()
{
	// Declare all pointers and variables needed, and allocate space using pointer variables for Node structure.
	Node *pHead = nullptr;
	int choice = 0;
	string FindTitle;
	Node *pFind;

	// Call CreateDVDLinkedList function to read the data file and create the linked list of DVD nodes
	CreateDVDLinkedList(pHead);

	while (choice != 5)
	{
		// Ask user to select from the following menu of options:
		cout << "1. Rent" << endl;
		cout << "2. Return" << endl;
		cout << "3. Find" << endl;
		cout << "4. Listing" << endl;
		cout << "5. Quit" << endl;
		cout << "Choice? [1, 2, 3, 4, or 5] ";
		cin >> choice;

		if (choice >= 1 && choice <= 5)
		{
			// Using case statement to do the user's option.
			switch (choice)
			{
			case 1:
				// Ask user to enter the title.
				cout << "Title? ";
				cin.ignore();
				getline(cin, FindTitle);

				// Call FindDVDbyTitle function to find the title that user entered and return pointer to node if found or not.
				pFind = FindDVDbyTitle(pHead, FindTitle);

				// Call CheckOut function to decrement number of DVDs currently in the store, if DVD owned by the store and if not 0 already
				if (pFind != nullptr)
					CheckOut(pFind);
				else
					cout << FindTitle << " not found in the library." << endl;
				break;

			case 2:
				// Ask user to enter the title.
				cout << "Title? ";
				cin.ignore();
				getline(cin, FindTitle);

				// Call FindDVDbyTitle function to find the title that user entered and return pointer to node if found or not.
				pFind = FindDVDbyTitle(pHead, FindTitle);

				// Call CheckIn function to increment number of DVDs currently in the store if DVD owned by the store
				if (pFind != nullptr)
					CheckIn(pFind);
				else
					cout << FindTitle << " not found in the library." << endl;
				break;

			case 3:
				// Ask user to enter the title.
				cout << "Title? ";
				cin.ignore();
				getline(cin, FindTitle);

				// Call FindDVDbyTitle function to find the title that user entered and return pointer to node if found or not.
				pFind = FindDVDbyTitle(pHead, FindTitle);

				// Call ShowDVDInformation function to display the DVD information that found.
				if (pFind != nullptr)
					ShowDVDInformation(pFind);
				else
					cout << FindTitle << " not found in the library." << endl;
				break;

			case 4:
				// Call DisplayDVDLibrary function to display all DVDs owned by the store (tabular form) 
				DisplayDVDLibrary(pHead);
				break;

			case 5:
				break;


			}
		}
		else
			cout << "You did not enter a number from 1 to 5, please try again! " << endl;
	}

	return 0;
}

// Function:	CreateDVDLinkedList   
// Description: read the data file and create the linked list of DVD nodes. 
// Returns:		void
void CreateDVDLinkedList(Node* &pHead)
{
	// Declare the file input object and open the file.
	ifstream infile;
	infile.open("lab4.txt");

	Node *pNew;
	Node *ptr;

	// Use if/else statement to check if the file opened successfully or not.
	if (infile)
	{
		// When the file opened successfully, using while loop to read in all the data and create the linked list of DVD nodes.
		while (!infile.eof())
		{
			// Create the linked list of DVD nodes.
			pNew = new Node;
			pNew->pNext = nullptr;

			// Read in all the data
			getline(infile, pNew->movie);
			getline(infile, pNew->director);
			infile >> pNew->rate;
			infile >> pNew->copies;
			infile.ignore();

			// Link the list of DVD nodes.
			if (pHead == nullptr)
				pHead = pNew;
			else 
			{
				ptr = pHead;
				while (ptr->pNext != nullptr)
					ptr = ptr->pNext;

				ptr->pNext = pNew;
			}
		}
	}
	else
		cout << "File opened unsuccessfully!" << endl;

	infile.close();
}

// Function:	ShowDVDInformation   
// Description: given pointer to the node, display the DVD information 
// Returns:		void
void ShowDVDInformation(Node* pFind)
{
	// Display the DVD information if found
	cout << left << setw(46) << "Title" << setw(15) << "Director" << setw(10) << "Rating" << setw(6) << "Count" << endl << endl;
	cout << left << setw(46) << pFind->movie << setw(15) << pFind->director << setw(10) << pFind->rate << setw(6) << pFind->copies << endl;
}

// Function:	DisplayDVDLibrary   
// Description: given pointer to the first node, display all DVDs owned by the store (tabular form) 
// Returns:		void
void DisplayDVDLibrary(Node* pHead)
{
	Node *ptr;

	ptr = pHead;

	// Using while loop to display all DVDs owned by the store (tabular form)
	cout << left << setw(46) << "Title" << setw(15) << "Director" << setw(10) << "Rating" << setw(6) << "Count" << endl << endl;
	while (ptr)
	{
		cout << left << setw(46) << ptr->movie << setw(15) << ptr->director << setw(10) << ptr->rate << setw(6) << ptr->copies << endl << endl;
		ptr = ptr->pNext;
	}
}

// Function:	FindDVDbyTitle   
// Description: given pointer to the first node and the title, find and return pointer to the appropriate node in the list or nullptr otherwise 
// Returns:		return pointer to the appropriate node in the list or nullptr otherwise
Node* FindDVDbyTitle(Node* pHead, string FindTitle)
{
	Node *ptr;
	ptr = pHead;
	Node *pReturn = nullptr;

	// Find and return pointer to the appropriate node in the list or nullptr otherwise
	while (ptr)
	{
		if (ptr->movie == FindTitle)
			pReturn = ptr;
		ptr = ptr->pNext;
	}
	return pReturn;
}

// Function:	CheckOut   
// Description: given pointer to the DVD node, decrement number of DVDs currently in the store, if DVD owned by the store and if not 0 already 
// Returns:		void
void CheckOut(Node* pFind)
{
	// Decrement number of DVDs currently in the store, if DVD owned by the store and if not 0 already
	if (pFind->copies > 0)
		pFind->copies--;
	else
		cout << "ERROR: All DVDs for this movie are currently rented out." << endl;
}

// Function:	CheckIn   
// Description: given pointer to the DVD node, increment number of DVDs currently in the store if DVD owned by the store  
// Returns:		void
void CheckIn(Node* pFind)
{
	// Increment number of DVDs currently in the store if DVD owned by the store
	pFind->copies++;
}

// Function:	DeleteList   
// Description: given pointer to the node, delete the linked list.
// Returns:		void
void DeleteList(Node *pHead)
{
	Node* pTemp;
	Node* pHold;
	pHold = nullptr;
	pTemp = pHead;

	// Using while loop to delete the linked list.
	while (pTemp != nullptr)
	{
		pHold = pTemp->pNext;
		delete pTemp;
		pTemp = pHold;
	}
}

