// Guard preprocessor statements
#ifndef LAB6_H
#define LAB6_H

// Circle class definition includes the private data members and setter and getter function prototypes.
class Circle
{
	// Public member functions
public:

	// Mutator functions.
	void setxPos(double);
	void setyPos(double);
	void setRadius(double);
	void moveCircle(double, double);
	// Accessor functions.
	double getxPos() const;
	double getyPos() const;
	double getRadius() const;

	void displayCircle() const;
	void displayQuad() const;

	// Private data members
private:
	double radius;
	double xPos;
	double yPos;

};

#endif 
