// Lab 6
// Description: This lab assignment is designed to give you practice working with C++ classes.
//				Declare a Circle class that describes the size and position of a Circle object on an x, y axis and create a demonstration
//				program to test it.
// Programmer:  Phuong Hai Nguyen
// Class:		CS 2020
// Semester:	Spring 2016

#include<iostream>
#include"lab6.h"
using namespace std;

int main()
{
	double x, y, r, moveX, moveY;
	// Define (instantiate) a Circle object called ring.
	Circle ring;

	// Prompt the user to enter a radius and x, y coordinates for the ring object and store these values in the ring's private data members.
	cout << "Please enter r, x, y: ";
	cin >> r;
	ring.setRadius(r);
	cin >> x;
	ring.setxPos(x);
	cin >> y;
	ring.setyPos(y);

	// Call displayCircle function to displays the radius, xPos and yPos values of a Circle object.
	ring.displayCircle();

	// Call displayQuad function to determine and display in which quadrant a Circle object is located.
	ring.displayQuad();

	// Prompt the user to enter an x move and y move value. Use these values to move the ring object on the x, y axis.
	cout << "Please enter delta x, delta y: ";
	cin >> moveX >> moveY;

	// Call moveCircle function to move the Circle object horizontally and vertically on the x, y axis.
	ring.moveCircle(moveX, moveY);

	// Call displayCircle function to displays the radius, xPos and yPos values of a Circle object (after moving).
	ring.displayCircle();

	// Call displayQuad function to determine and display in which quadrant a Circle object is located (after moving).
	ring.displayQuad();

	system("pause");
	return 0;
}