// Description: hold the function implementations for the two additional member functions described below
#include<iostream>
#include "lab6.h"
using namespace std;

// Function:	setxPos   
// Returns:		void
void Circle::setxPos(double x)
{
	xPos = x;
}

// Function:	setyPos   
// Returns:		void
void Circle::setyPos(double y)
{
	yPos = y;
}

// Function:	setRadius   
// Returns:		void
void Circle::setRadius(double r)
{
	radius = r;
}

// Function:	getxPos   
// Returns:		double
double Circle::getxPos() const
{
	return xPos;
}

// Function:	getyPos   
// Returns:		double
double Circle::getyPos() const
{
	return yPos;
}

// Function:	getRadius   
// Returns:		double
double Circle::getRadius() const
{
	return radius;
}

// Function:	displayCircle   
// Description: displays the radius, xPos and yPos values of a Circle object.
// Returns:		void
void Circle::displayCircle() const
{
	cout << "The Circle object with radius " << radius << " has coordinates (" << xPos << " ," << yPos << ")" << endl;
}

// Function:	moveCircle   
// Description: has two parameters used to move the Circle object horizontally and vertically on the x, y axis
//				-The first parameter indicates how much to move the circle to the right(if the value is positive)
//				or left(if the value is negative).Do not use any if statements here!
//				- The second parameter indicates how much to move the circle up(if the value is positive)
//				or down(if the value is negative).
// Returns:		void
void Circle::moveCircle(double moveX, double moveY)
{
	xPos += moveX;
	yPos += moveY;
}

// Function:	displayQuad
// Description: determine and display in which quadrant a Circle object is located.
// Returns:		void
void Circle::displayQuad() const
{
	if (xPos > 0 && yPos > 0)
		cout << "The Circle object is in Quadrant 1." << endl;
	else if (xPos > 0 && yPos < 0)
		cout << "The Circle object is in Quadrant 4." << endl;
	else if (xPos < 0 && yPos < 0)
		cout << "The Circle object is in Quadrant 3." << endl;
	else if (xPos == 0 && yPos == 0)
		cout << "The Circle object is on origin point (0, 0)." << endl;
	else if (xPos < 0 && yPos > 0)
		cout << "The Circle object is in Quadrant 2." << endl;
	else if (xPos == 0 && yPos < 0)
		cout << "The Circle object is on y-axis (between Quadrant 3 and Quadrant 4)." << endl;
	else if (xPos == 0 && yPos > 0)
		cout << "The Circle object is on y-axis (between Quadrant 1 and Quadrant 2)." << endl;
	else if (xPos < 0 && yPos == 0)
		cout << "The Circle object is on x-axis (between Quadrant 2 and Quadrant 3)." << endl;
	else
		cout << "The Circle object is on x-axis (between Quadrant 1 and Quadrant 4)." << endl;
}