// Program 6	(Due: April 11 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// All member function definitions in a file named prog6.cpp.
#include<iostream>
#include "prog6.h"
using namespace std;

// Function:	simplify   
// Description: This function should checks for values in cents that are 100 or greater or less than 0, adjusts dollars 
//				and cents as needed so that cents amount is never greater than 99
// Returns:		void
void Money::simplify()
{
	// Using if/else if statements to checks for values in cents that are 100 or greater or less than 0, adjusts dollars 
	//				and cents as needed so that cents amount is never greater than 99
	if (cents >= 100)
	{
		while (cents >= 100)
		{
			cents -= 100;
			dollars++;
		}
	}
	else if (cents < 0)
	{
		while (cents < 0)
		{
			cents = 100 - cents;
			dollars--;
		}
	}

}

// Function:	Money
// Description: sets dollars and cents to zero
// Return:		default constructor
Money::Money()
{
	dollars = 0;
	cents = 0;
}

// Function:	Money
// Description: sets dollars and cents to the values passed to the constructor
// Return:		another constructor
Money::Money(int dollars, int cents)
{
	if ((dollars > 0 && cents < 0) || (dollars < 0 && cents > 0))
	{
		cout << "Invalid money amount!" << endl;
		exit(1);
	}

	this->dollars = dollars;
	this->cents = cents;
	
	// Call simplify function to adjusts dollars and cents as needed
	simplify();
}

// Function:	setDollars
// Description: sets dollars values
// Return:		void
void Money::setDollars(int dollars)
{
	this->dollars = dollars;
}

// Function:	setCents
// Description: sets cents values
// Return:		void
void Money::setCents(int cents)
{
	this->cents = cents;
}

// Function:	getDollars
// Description: get the dollars values
// Return:		int
int Money::getDollars() const
{
	return dollars;
}

// Function:	getCents
// Description: get the cents values
// Return:		int
int Money::getCents() const
{
	return cents;
}

// Function:	operator+
// Description: adds two Money objects together
// Return:		Money object
Money Money::operator+ (const Money &amount)
{
	// Declare a temporary Money object
	Money temp;

	temp.cents = this->cents + amount.cents;
	temp.dollars = this->dollars + amount.dollars;

	// Call simplify function to adjusts dollars and cents as needed
	temp.simplify();

	return temp;
}

// Function:	operator-
// Description: subtracts two Money objects
// Return:		Money object
Money Money::operator- (const Money &amount)
{
	// Declare a temporary Money object
	Money temp;

	temp.cents = this->cents - amount.cents;
	temp.dollars = this->dollars - amount.dollars;

	// Call simplify function to adjusts dollars and cents as needed
	temp.simplify();

	return temp;
}

// Function:	operator<<
// Description: displays a Money object in the form $dd.cc
// Return:		Display output object
ostream & operator<< (ostream &osObject, const Money &amount)
{
	osObject << "$" << amount.dollars << ".";
	if (amount.cents < 10)
		osObject << "0";
	osObject << amount.cents;

	return osObject;
}

// Function:	operator>>
// Description: accepts values for a Money object entered as a double value (e.g., 5.25) 
//				and stores the dollars and cents amounts correctly in the Money object's data members
// Return:		Input object
istream & operator>> (istream &isObject, Money &amount)
{
	double money = 0;
	isObject >> money;

	money = money * 100;
	amount.dollars = money / 100;
	amount.cents = money - amount.dollars * 100;

	// Call simplify function to adjusts dollars and cents as needed
	amount.simplify();

	return isObject;
}

// Function:	operator++
// Description: using the ++ operator (prefix) to add 1 cent each time to the first Money object
// Return:		this pointer
Money Money::operator++()
{
	++cents;
	if (cents >= 100)
	{
		++dollars;
		cents -= 100;
	}
	return *this;
}

// Function:	operator+=
// Description: Add 1.99 to the second Money object with the += operator
// Return:		this pointer
Money & Money::operator+= (double add)
{
	int addCents = add * 100;
	int addDollars = addCents / 100;
	this->cents += (addCents - addDollars * 100);
	this->dollars += addDollars;

	// Call simplify function to adjusts dollars and cents as needed
	simplify();

	return *this;
}