// Program 6	(Due: April 11 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// Description: Write a complete C++ program to define and demonstrate a Money class. 
//				Dollars and cents will be stored separately so that money amounts can be represented as precisely as possible.
#include<iostream>
#include "prog6.h"
using namespace std;

// Main function
int main()
{
	// Instantiate four Money objects: one with the values 7 and 95 for dollars and cents, 
	// one with values 4 and 90 for the dollars and cents and the third and fourth with default values.
	Money amount1(7, 95);
	Money amount2(4, 90);
	Money amount3;
	Money amount4;
	
	// Display the value of all four Money objects using the overloaded << operator function of the Money class.
	cout << "The value of money1 is: " << amount1 << endl;
	cout << "The value of money2 is: " << amount2 << endl;
	cout << "The value of money3 is: " << amount3 << endl;
	cout << "The value of money4 is: " << amount4 << endl;
	cout << endl;

	// Add the first two Money objects together, storing the result in the third Money object
	amount3 = amount1 + amount2;

	// Display the result with an appropriate label.
	cout << "money1 (" << amount1 << ") plus money2 (" << amount2 << ") = money3 (" << amount3 << ")" << endl;

	// Subtract the first two Money objects (subtract second from first), storing the result in the fourth Money object.
	amount4 = amount1 - amount2;

	// Display the result with an appropriate label.
	cout << "money1 (" << amount1 << ") minus money2 (" << amount2 << ") = money4 (" << amount4 << ")" << endl << endl;

	// Display the result with an appropriate label.
	cout << "Enter a value for money3 (e.g: 5.25): "; 
	cin >> amount3;

	// Display the value stored in the third Money object.
	cout << "The value of money3 now is: " << amount3 << endl << endl;

	// Using a for loop to repeat 10 times, using the ++ operator (prefix) to add 1 cent each time to the first Money object.
	for (int count = 0; count < 10; count++)
		++amount1;

	// Display its value after the loop.
	cout << "The value of money 1 after adding 1 cent 10 times is: " << amount1 << endl;

	// Add 1.99 to the second Money object with the += operator
	amount2 += 1.99;

	// Display the result.
	cout << "The value of money 2 after adding $1.99 is: " << amount2 << endl;

	return 0;
}