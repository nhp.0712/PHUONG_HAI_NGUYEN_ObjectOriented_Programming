// Program 6	(Due: April 11 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// The class declaration with data members and member function prototypes into a file named prog6.h.

// Guard preprocessor statements
#ifndef PROG6_H
#define PROG6_H
#include<iostream>
using namespace std;

// Create a class called Money with the data members and member functions
class Money
{
	// Private data members:
private:
	int dollars;
	int cents;

	// Private data members:
	void simplify();

	// Public member functions:
public:

	// Default constructor
	Money();

	// A second constructor
	Money(int , int );

	// Mutator functions
	void setDollars(int );
	void setCents(int );

	// Accessor functions
	int getDollars() const;
	int getCents() const;

	// Overloaded operator functions
	Money operator+ (const Money &);
	Money operator- (const Money &);
	friend ostream &operator<< (ostream &, const Money &);
	friend istream &operator>> (istream &, Money &);
	Money operator++ ();
	Money & operator+= (double );
};

#endif 