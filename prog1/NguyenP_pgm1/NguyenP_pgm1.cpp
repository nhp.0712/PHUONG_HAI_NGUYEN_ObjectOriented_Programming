// Program 1   
// Description: Create a program plan and then convert it into C++ statements.
//              Practice debugging, declaring variables, file I/O, functions, arrays, sorting and searching arrays.      
// Programmer:  Phuong Hai Nguyen
// Class:		CS 2020
// Semester:	Spring 2016


#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
using namespace std;

const int MAX_INDEX = 30;

// Function prototypes
void ReadData(string [], int [], string [], double [], int &);
void BSort(string[], int[], string[], double[], int);
void PrintArrays(string[], int[], string[], double[], int);
void Restock(string[], int[], int);
int HiPrice(double[], int);
void AverWarehouse(string [], double[], double &, double &, double &, int);

int main()
{
// Declare all arrays and variables needed 
	int index = 0;
	string item[MAX_INDEX], location[MAX_INDEX];
	int qty[MAX_INDEX];
	double price[MAX_INDEX];
	int subs;
	double averM = 0, averC = 0, averJ = 0;

// Call the ReadData Function to read the data and populate the arrays.
	ReadData(item, qty, location, price, index);

// Call the BSort Function to sort all four arrays from highest to lowest by quantity. 	
	BSort(item, qty, location, price, index);

// Call the PrintArrays Function that prints the four arrays in tabular form with column headers.
	PrintArrays(item, qty, location, price, index);

// Call the Restock Function to print each product that is under stock. 
	Restock(item, qty, index);

// Call the HiPrice Function to return the subscript of the highest price product in stock. 
	subs = HiPrice(price, index);

// Display the item name, location and price of the highest price product in stock.
	cout << "The highest priced item is " << item[subs] << ",located in " << location[subs] << " with a price of $" << price[subs] << endl << endl;

// Call the AverWarehouse Function to return the average prices for each of the three warehouses.
	AverWarehouse(location, price, averM, averC, averJ, index);

// Display the average prices for all three warehouses. 
	cout << fixed << setprecision(2);
	cout << "Warehouse C average:  $" << averC << endl;
	cout << "Warehouse J average:  $" << averJ << endl;
	cout << "Warehouse M average:  $" << averM << endl;
	cout << endl;

	return 0;
}


// Function:	ReadData   
// Description: Call the ReadData Function to read the data and populate the arrays  
//				a.Pass the arguments needed 
//				b.Open the file prog1.txt inside this function.
//				c.Use a while loop to read data into the arrays until EOF is reached 
//				d.close the file
// Returns:		void
void ReadData(string item[], int qty[], string location[], double price[], int &index)
{
	ifstream infile;
	infile.open("prog1.txt");

	if (infile)
	{
		
		while (!infile.eof() && index < MAX_INDEX)
		{
			getline(infile, item[index], '\n');
			infile >> qty[index];
			infile.ignore();
			getline(infile, location[index], '\n');
			infile >> price[index];
			infile.ignore();
			index++;
		

		}
	}
	else
		cout << " File opened error." << endl;

	infile.close();
}


// Function:	BSort 
// Description: Use the bubble sort to sort all four arrays simultaneously using the qty array as they key. 
// Returns:		void
void BSort(string item[], int qty[], string location[], double price[], int index)
{
	bool stub;
	int a;
	double b;
	string x, y;

	do
	{
		stub = false;

		for (int count = 0; count < (index - 1); count++)
		{
			if (qty[count] < qty[count + 1])
			{
				a = qty[count];
				qty[count] = qty[count + 1];
				qty[count + 1] = a;

				b = price[count];
				price[count] = price[count + 1];
				price[count + 1] = b;

				x = item[count];
				item[count] = item[count + 1];
				item[count + 1] = x;

				y = location[count];
				location[count] = location[count + 1];
				location[count + 1] = y;

				stub = true;

			}
			
		}
	} while (stub);
}


// Function:	PrintArrays 
// Description: Print the arrays in tubular form using the setw function. 
// Returns:		void
void PrintArrays(string item[], int qty[], string location[], double price[], int index)
{
	cout << left << setw(15) << "Item" << setw(8) << "Qty" << setw(16) << "Location" << setw(10) << "Price" << endl;
	cout << left << setw(15) << "----" << setw(8) << "---" << setw(16) << "--------" << setw(10) << "-----" << endl;

	for (int i = 0; i < index; i++)
		cout << left << setw(15) << item[i] << setw(8) << qty[i] << setw(16) << location[i] << setw(10) << price[i] << endl;

	cout << endl << endl;
}


// Function:	Restock  
// Description: A product is under stock if it has fewer than 100 items in stock. Display the product item name and quantity in a tabular form.  
// Returns:     void
void Restock(string item[], int qty[], int index)
{
	cout << "The low stock items are: " << endl;
	cout << left << setw(15) << "Item" << setw(8) << "Qty" << endl;
	cout << left << setw(15) << "----" << setw(8) << "---" << endl;

	for (int count = 0; count < index; count++)
	{
		if (qty[count] < 100)
			cout << left << setw(15) << item[count] << setw(8) << qty[count] << endl;
	}
	cout << endl;
}


// Function:	HiPrice
// Description: Find the highest price product in stock
// Returns:		Return the subscript of the highest price product in stock. 
int HiPrice(double price[], int index)
{
	double hiprice;
	int count, a = 0;
	hiprice = price[0];

	for (count = 1; count < index; count++)
	{
		if (price[count] > hiprice)
			a = count;
	}
	return a;
}


// Function:	AverWarehouse 
// Description: Find the average prices for each of the three warehouses.
// Returns:		return all three averages via reference parameters.
void AverWarehouse(string location[], double price[], double &averM, double &averC, double &averJ, int index)
{
	double totalM = 0, totalC = 0, totalJ = 0;
	double x = 0 , y = 0, j = 0;

	for (int count = 0; count < index; count++)
	{
		if (location[count] == "Warehouse M")
		{
			totalM += price[count];
			x++;
		}
		else if (location[count] == "Warehouse C")
		{
			totalC += price[count];
			y++;
		}
		else
		{
			totalJ += price[count];
			j++;
		}
	}

	 averM = totalM / x;
	 averC = totalC / y;
	 averJ = totalJ / j;
}