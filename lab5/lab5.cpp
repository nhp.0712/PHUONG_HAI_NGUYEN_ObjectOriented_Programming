// Lab 5
// Description: This lab assignment is designed to give you practice working with C++ classes. 
//				Each object in on the nice list contains child's name (string) and a pointer to the first node of that child's list of 
//				gifts (an instance of ADT list). Below are definitions for each child node on the nice list and each gift node for the 
//				gift list. Below is a visual representation of the nice list containing one child node, Jennifer, with two gifts on her 
//				list, Barbie and iPad. Maintain child names in alphabetic order as they are inserted into the list. Do not maintain 
//				any order for the gifts 
// Programmer:  Phuong Hai Nguyen
// Class:		CS 2020
// Semester:	Spring 2016
#include<iostream>
#include<string>
#include<fstream>
#include<iomanip>

using namespace std;

// Define a Gift structure that contains: desc (gifts)
// Then declare a Next pointer in structure.
struct Gift
{
	string desc;
	Gift*  pNext;
};

// Define a Nice structure that contains: name of child
// Then declare a Next pointer in structure and the a Head pointer of Gift list in this structure.
struct Nice
{
	string name;
	Nice*  pNext;
	Gift*  pGiftHead;

};

// Function prototypes 
void createList(Nice* &);
void appendGift(Gift* &, Gift *);
void insertNice(Nice* &, Nice*);
void printGifts(Nice*, string);
void printNiceList(Nice*);

// Main function
int main()
{
	// Declare all pointers and variables needed, and allocate space using pointer variables for Nice structure.
	Nice *pHead = nullptr;
	int choice = 0;
	string find;

	// Call the createList function to create the nice list 
	createList(pHead);

	// Ask user to select from the following menu of options:
	while (choice != 3)
	{
		cout << "===============" << endl;
		cout << "1. Print nice" << endl;
		cout << "2. Print gifts" << endl;
		cout << "3. Quit" << endl;
		cout << "Choice: ";
		cin >> choice;
		cout << "===============" << endl;

		if (choice == 1)

			// Call printNiceList function to display the nice list
			printNiceList(pHead);
		else if (choice == 2)
		{
			cout << "Gifts for whom? ";
			cin >> find;

			// Call printGifts function to display the gift list
			printGifts(pHead, find);
		}
		else if (choice == 3)
			break;

		else
		{
			cout << "ERROR: invalid choice. Please re-enter." << endl;
		}
	}


	return 0;
}

// Function:	createList   
// Description: Create the nice list 
// Returns:		void
void createList(Nice* &pHead)
{

	// Declare the file input object and open the file.
	ifstream infile;
	infile.open("lab5.txt");

	Nice *pNew;
	Gift * pNewGift;

	// Use if/else statement to check if the file opened successfully or not.
	if (infile)
	{
		// When the file opened successfully, using while loop to read in all the data and store into the list
		while (!infile.eof())
		{

			string s, ss;

			int j = 0;
			int i = 1;

			pNew = new Nice;
			pNew->pNext = nullptr;
			pNew->pGiftHead = nullptr;

			// Read the data
			getline(infile, s);
			s = s + " ";

			while (s.length() > 1)
			{

				j = s.find(" ");
				ss = s.substr(0, j);

				if (i == 1)
				{
					// Declare the value into name variable.
					pNew->name = ss;
					// Call the insertNice function to insert into the nice list
					insertNice(pHead, pNew);
				}
				else
				{
					pNewGift = new Gift;
					pNewGift->pNext = nullptr;

					// Declare the value into gift variable.
					pNewGift->desc = ss;
					// Call the appendGift function to append to a gift list
					appendGift(pNew->pGiftHead, pNewGift);
				}

				i++;
				s = s.substr(j + 1);
			}
		}

	}
	else
		cout << "File opened unsuccessfully!" << endl;

	infile.close();
}

// Function:	appendGift   
// Description: Append to a gift list  
// Returns:		void
void appendGift(Gift* &pGiftHead, Gift* pNewGift)
{
	Gift * pTemp;

	// Link the Gift list
	if (!pGiftHead)
		pGiftHead = pNewGift;
	else
	{
		pTemp = pGiftHead;

		while (pTemp->pNext)
			pTemp = pTemp->pNext;
		pTemp->pNext = pNewGift;

	}
}

// Function:	insertNice   
// Description: Insert into the nice list  
// Returns:		void
void insertNice(Nice* &pHead, Nice* pNew)
{
	Nice* pAfter = pHead;
	Nice* pBefore = nullptr;

	// Link the Nice list
	if (!pHead)
	{
		pHead = pNew;
		pHead->pNext = nullptr;
	}
	else
	{
		pAfter = pHead;
		pBefore = nullptr;


		while (pAfter != nullptr && pAfter->name < pNew->name)
		{

			pBefore = pAfter;
			pAfter = pAfter->pNext;

		}

		if (pAfter == pHead)
		{
			pNew->pNext = pHead;
			pHead = pNew;
		}
		else
		{
			pNew->pNext = pAfter;
			pBefore->pNext = pNew;
		}

	}
}

// Function:	printNiceList   
// Description: Display the nice list 
// Returns:		void
void printNiceList(Nice* pHead)
{
	Nice *ptr;
	ptr = pHead;
	int a = 0;

	while (ptr)
	{
		cout << right << setw(2) << ++a << ": " << ptr->name << endl;
		ptr = ptr->pNext;
	}
}

// Function:	printGifts   
// Description: Display a gift list  
// Returns:		void
void printGifts(Nice *pHead, string find1)
{
	Nice *ptr;
	ptr = pHead;
	Gift *ptr1;
	ptr1 = ptr->pGiftHead;
	bool flag = false;
	int i = 0;

	// Find the name of child and display the output if found or not.
	while (ptr)
	{
		if (ptr->name == find1)
		{
			flag = true;
			ptr1 = ptr->pGiftHead;
			while (ptr1)
			{

				cout << ptr1->desc;
				if (ptr1->pNext)
					cout << ", ";
				else
					cout << endl;
				ptr1 = ptr1->pNext;
				i++;
			}
			if (i == 0)
				cout << find1 << "'s gift list is empty." << endl;

		}

		ptr = ptr->pNext;
	}
	if (!flag)
		cout << find1 << " not on the list." << endl;
}

