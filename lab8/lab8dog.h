// Lab 8		(Due: April 18 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// The base class Dog is a class with general information about dogs. 
// Store the declaration for this class in a file called lab8dog.h

// Guard preprocessor statements
#ifndef LAB_8DOG
#define LAB_8DOG
#include<iostream>
#include<string>
using namespace std;

// Create a class called Dog with the data members and member functions
class Dog
{
	// Protected data members
protected:
	string name;
	string color;
	double weight;

	// Public member functions:

public:

	// Default constructor
	Dog();

	// Overloaded constructor
	Dog(string, string, double);

	// Accessor function
	virtual void displayDog() const;

	// Destructor function
	virtual ~Dog(){ };
};


#endif