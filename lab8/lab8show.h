// Lab 8		(Due: April 18 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// Store the declaration for the ShowDog class in the file lab8show.h

// Guard preprocessor statements
#ifndef LAB_8SHOW
#define LAB_8SHOW
#include<iostream>
#include<string>
#include "lab8dog.h"
using namespace std;

// Create a derived class with additional information about dogs with the data members and member functions
class ShowDog:public Dog
{
	// Private data members:
private:
	int totalCon;
	int wonCon;

	// Public member functions:
public:

	// Default constructor
	ShowDog();

	// Overloaded constructor
	ShowDog(string, string, double, int, int);

	// Accessor function
	void displayDog() const;
};


#endif 
