// Lab 8		(Due: April 18 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// Description: Using C++ inheritance to create a base class and a derived class and working with constructors and redefining a class member function.
#include<iostream>
#include<string>
#include<iomanip>
#include"lab8dog.h"
#include"lab8show.h"
using namespace std;

// Main function
int main()
{
	// Define a Dog object initializing the name, color and weight with "Bolt", "White" and 20.
	Dog dog1("Bolt", "White", 20);

	// Define a ShowDog object with arguments for all five data members using the values "Uggie", "Blk/Wht", 30, 3, 1.
	ShowDog dog2("Uggie", "Blk/Wht", 30, 3, 1);

	// Call the displayDog function to display the information about each of these objects
	dog1.displayDog();
	cout << endl;
	dog2.displayDog();
	cout << endl;

	// Define an array with four elements to hold pointers of type Dog.
	const int ARRAY_SIZE = 4;
	Dog *dogList[ARRAY_SIZE];

	// Using the new operator to create either a Dog or ShowDog object, storing the pointer to the object (an address) in the next element of the array.
	dogList[0] = new Dog("Bingo", "Tan", 20);
	dogList[1] = new ShowDog("Beethoven", "Brown", 200, 3, 1);
	dogList[2] = new ShowDog("Marley", "Yellow", 80, 10, 3);
	dogList[3] = new Dog("Fido", "Rust", 50);

	// Using a for loop to display the data for each Dog or ShowDog in the array.
	for (int count = 0; count < ARRAY_SIZE; count++)
	{
		dogList[count]->displayDog();
		cout << endl;
	}

	return 0;
}