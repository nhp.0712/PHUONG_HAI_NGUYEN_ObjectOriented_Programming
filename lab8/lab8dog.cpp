// Lab 8		(Due: April 18 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// All member function definitions for Dog class in a file named lab8dog.cpp
#include<iostream>
#include<string>
#include<iomanip>
#include"lab8dog.h"
using namespace std;

// Function:	Dog
// Description: Initialize name and color to Unknown, weight to zero
// Return:		default constructor
Dog::Dog()
{
	name = "Unknown";
	color = "Unknow";
	weight = 0.0;
}

// Function:	Dog
// Description: Accepts values for the dog's name, color and weight and assigns them to the data members
// Return:		overloaded constructor
Dog::Dog(string name, string color, double weight)
{
	this->name = name;
	this->color = color;
	this->weight = weight;
}

// Function:	displayDog
// Description: Display the values of each data member.
// Return:		void
void Dog::displayDog() const
{
	cout << fixed << showpoint << setprecision(2);
	cout << "Dog's name:   " << name << endl;
	cout << "Dog's color:  " << color << endl;
	cout << "Dog's weight: " << weight << endl;

}