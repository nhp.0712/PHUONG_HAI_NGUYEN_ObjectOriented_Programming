// Lab 8		(Due: April 18 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// All member function definitions for ShowDog class in a file named lab8show.cpp
#include<iostream>
#include<string>
#include"lab8show.h"
using namespace std;

// Function:	ShowDog
// Description: Sets these two fields to zero and invokes the Dog default constructor to initialize the inherited data members
// Return:		default constructor
ShowDog::ShowDog():Dog()
{
	totalCon = 0;
	wonCon = 0;
}

// Function:	ShowDog
// Description: Accepts a dog's name, color, weight, number of contests entered and number won. 
//				Pass the first three arguments to the Dog constructor and use the last two to set the ShowDog's data members.
// Return:		overloaded constructor
ShowDog::ShowDog(string name, string color, double weight, int totalCon, int wonCon) :Dog(name, color, weight)
{
	this->totalCon = totalCon;
	this->wonCon = wonCon;
}

// Function:	displayDog
// Description: This function is redefining the base class version of displayDog. It will first call the base class's function 
//				(to display the dog's name, color and weight) and then display the number of contests entered by the dog and the number won
// Return:		void
void ShowDog::displayDog() const
{
	Dog::displayDog();
	cout << name << " was entered in " << totalCon << " contests," << endl;
	cout << name << " won " << wonCon << " of the contests." << endl;
}