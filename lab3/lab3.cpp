#include<iostream>
#include<string>
#include<iomanip>
#include<fstream>

using namespace std;

struct Shareholder
{
	string name;
	string type;
	int number;
};

const int MAX_Numclients = 50;

void fillArray(Shareholder Clientlist[], int &);
void printArray(Shareholder Clientlist[], string, int);
void breakdown(Shareholder Clientlist[], int);
int main()
{
	int numclients = 0;
	string cate;

	Shareholder Clientlist[MAX_Numclients];
	fillArray(Clientlist, numclients);
	cout << "Share type [Gold : Silver : Bronze : Done]: ";
	cin >> cate;

	while (cate != "Done")
	{
		if (cate == "Gold" || cate == "Silver" || cate == "Bronze")

		{
			cout << "===================================" << endl;
			cout << "       " << cate << " Clients     " << endl;
			cout << "===================================" << endl;

			printArray(Clientlist, cate, numclients);
			cout << endl;
			cout << "Share type [Gold : Silver : Bronze : Done]: ";
			cin >> cate;
		}
		else
		{
			cout << "ERROR: Invalid type!" << endl;
			cout << "Share type [Gold : Silver : Bronze : Done]: ";
			cin >> cate;
		}

	}

	breakdown(Clientlist, numclients);

	system("pause");
	return 0;
}

void fillArray(Shareholder  Clientlist[], int &numclients)
{
	ifstream infile;
	int i = 0;
	infile.open("lab3.txt");

	while (!infile.eof())
	{
		getline(infile, Clientlist[i].name);
		getline(infile, Clientlist[i].type);
		infile >> Clientlist[i].number;
		infile.ignore();
		i++;
		numclients = i;
	}
	infile.close();

}

void printArray(Shareholder Clientlist[], string cate, int numclients)
{
	int highestNum = 0;
	string highestName;
	for (int i = 0; i < numclients; i++)
	{
		if (cate == "Gold")
		{


			if (Clientlist[i].type == "Gold")
			{
				if (highestNum < Clientlist[i].number)
				{
					highestNum = Clientlist[i].number;
					highestName = Clientlist[i].name;
				}
				cout << left << setw(20) << Clientlist[i].name << setw(20) << Clientlist[i].number << endl;
			}
		}

		if (cate == "Silver")
		{
			if (Clientlist[i].type == "Silver")
			{
				if (highestNum < Clientlist[i].number)
				{
					highestNum = Clientlist[i].number;
					highestName = Clientlist[i].name;
				}
				cout << left << setw(20) << Clientlist[i].name << setw(20) << Clientlist[i].number << endl;
			}
		}

		if (cate == "Bronze")
		{
			if (Clientlist[i].type == "Bronze")
			{
				if (highestNum < Clientlist[i].number)
				{
					highestNum = Clientlist[i].number;
					highestName = Clientlist[i].name;
				}
				cout << left << setw(20) << Clientlist[i].name << setw(20) << Clientlist[i].number << endl;
			}
		}

	}

		cout << ">>>> " << highestName << " has most shares with " << highestNum << endl;
}
void breakdown(Shareholder Clientlist[], int numclients)
{
	int quanG = 0, quanS = 0, quanB = 0;
	double valueG = 0, valueS = 0, valueB = 0;

	for (int i = 0; i < numclients; i++)
	{
		if (Clientlist[i].type == "Gold")
			quanG += Clientlist[i].number;
		if (Clientlist[i].type == "Silver")
			quanS += Clientlist[i].number;
		if (Clientlist[i].type == "Bronze")
			quanB += Clientlist[i].number;
	}

	valueG = quanG * 9.65;
	valueS = quanS * 4.75;
	valueB = quanB * 1.85;

	cout << "===================================" << endl;
	cout << left << setw(15) << "Share Type" << setw(15) << "Quantity" << setw(15) << "Value" << endl;
	cout << "===================================" << endl;
	cout << left << setw(17) << "Gold" << setw(13) << quanG << setw(15) << valueG << endl;
	cout << left << setw(17) << "Silver" << setw(13) << quanS << setw(15) << valueS << endl;
	cout << left << setw(17) << "Bronze" << setw(13) << quanB << setw(15) << valueB << endl;
}