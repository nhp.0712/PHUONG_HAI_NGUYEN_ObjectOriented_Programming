// Lab 7		(Due: April 11 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// Description: More practice writing programs that use classes. Creating a C++ program that uses a Time class.
#include<iostream>
#include<iomanip>
#include "lab7.h"
using namespace std;

// Main function
int main()
{
	// Instantiate three Time objects named FreeTime, WrongTime and BGTime.
	// Initialize WrongTime to 25:45 and BGTime to 7 : 30 during instantiation.
	Time FreeTime;
	Time WrongTime(25, 45);
	Time BGTime(7, 30);
	int HH = 0, MM = 0;

	// Display all three times.
	cout << right << setw(20) << "Free time: ";
	FreeTime.display();
	cout << endl << endl;
	cout << right << setw(20) << "Wrong time: ";
	WrongTime.display();
	cout << endl << endl;
	cout << right << setw(20) << "BG time: ";
	BGTime.display();
	cout << endl << endl;

	// User set time for the FreeTime object using the keyboard input
	cout << "Enter your free time as HH and MM: ";
	cin >> HH >> MM;
	FreeTime.setTime(HH, MM);
	cout << endl;

	// Display user's FreeTime
	cout << right << setw(20) << "Free time: ";
	FreeTime.display();
	cout << endl << endl;

	// Using for loop to add 150 minutes to your FreeTime object using your addOneMinute function
	for (int count = 0; count < 150; count++)
		FreeTime.addOneMinute();

	// Display FreeTime after adding 150 minutes
	cout << right << setw(47) << "Free time (after adding 150 minutes): ";
	FreeTime.display();
	cout << endl << endl;

	// Instantiate another Time object, ExtraTime. Initialize it to 1 hour and 15 minutes using the overloaded constructor
	Time ExtraTime(1, 15);

	// Display FreeTime before addition.
	cout << right << setw(46) << "Free time (before adding ExtraTime): ";
	FreeTime.display();
	cout << endl << endl;

	// Add ExtraTime to FreeTime
	FreeTime = FreeTime + ExtraTime;

	// Display FreeTime after addition.
	cout << right << setw(45) << "Free time (after adding ExtraTime): ";
	FreeTime.display();
	cout << endl << endl;

	// Using if statements to check if two Time objects are equal.
	if (FreeTime == WrongTime)
		cout << "Free time object and Wrong time object are equal!" << endl;
	if (FreeTime == BGTime)
		cout << "Free time object and BG time object are equal!" << endl;
	if (FreeTime == ExtraTime)
		cout << "Free time object and Extra time object are equal!" << endl;
	if (BGTime == WrongTime)
		cout << "Wrong time object and BG time object are equal!" << endl;
	if (ExtraTime == WrongTime)
		cout << "Wrong time object and Extra time object are equal!" << endl;
	if (BGTime == ExtraTime)
		cout << "BG time object and Extra time object are equal!" << endl;
	if (!(FreeTime == WrongTime) && !(FreeTime == BGTime) && !(FreeTime == ExtraTime) && !(BGTime == WrongTime) && !(ExtraTime == WrongTime) && !(BGTime == ExtraTime))
		cout << "There is no equal time object!" << endl;

	return 0;

}