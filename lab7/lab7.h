// Lab 7		(Due: April 11 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// The class declaration with data members and member function prototypes into a file named prog6.h.

// Guard preprocessor statements
#ifndef LAB7_H
#define LAB7_H
#include<iostream>
using namespace std;

// Create a class called Time with the data members and member functions
class Time
{
	// Private data members:
private:
	int hour;
	int minute;

	// Public member functions:
public:

	// Default constructor
	Time();

	// Overloaded constructor
	Time(int, int );

	// Mutator functions
	void setTime(int, int);
	void addOneMinute();

	// Accessor functions
	int getHour() const;
	int getMinute() const;
	void display() const;

	// Overloaded operator functions
	Time operator+ (const Time &);
	bool operator== (const Time &clock);
};

#endif