// Lab 7		(Due: April 11 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// All member function definitions in a file named prog6.cpp.
#include<iostream>
#include "lab7.h"
using namespace std;

// Function:	Time
// Description: Initialize hour and minute to zero
// Return:		default constructor
Time::Time()
{
	hour = 0;
	minute = 0;
}

// Function:	Time
// Description: Initialize hour and minute to parameters or to all zeros if either value is negative, or out of the range, greater than 23 for hours or greater than 59 for minutes
// Return:		overloaded constructor
Time::Time(int hour, int minute)
{
	this->hour = hour;
	this->minute = minute;
	if (hour < 0 || hour > 23 || minute < 0 || minute > 59)
	{
		this->hour = 0;
		this->minute = 0;
	}
		
}

// Function:	setTime
// Description: Set the object�s hour and minute or set both to zeros if either one value is negative or hours is greater than 23 or minutes is greater than 59.
// Return:		void
void Time::setTime(int hour, int minute)
{
	this->hour = hour;
	this->minute = minute;
	if (hour < 0 || hour > 23 || minute < 0 || minute > 59)
	{
		cout << "Invalid hour and minute value. Time becomes 00:00." << endl;
		this->hour = 0;
		this->minute = 0;
	}
}

// Function:	getHour
// Description: Returns the hour for the object
// Return:		int
int Time::getHour() const
{
	return hour;
}

// Function:	getMinute
// Description: Returns the minute for the object
// Return:		int
int Time::getMinute() const
{
	return minute;
}

// Function:	addOneMinute
// Description: Adds one minute to the current time (hint: account for 7:59 it should change to 8:00) or 23:59 it should change to the 0:00.
// Return:		void
void Time::addOneMinute()
{
	minute++;
	if (minute > 59)
	{
		hour++;
		minute = 0;
		if (hour > 23)
			hour = 0;
	}
}

// Function:	display
// Description: Displays time in the form HH:MM
// Return:		void
void Time::display() const
{

	if (hour < 10)
		cout << "0";
	cout << getHour() << ":";
	if (minute < 10)
		cout << "0";
	cout << getMinute();
}

// Function:	operator+
// Description: adds two Time objects together
// Return:		Time object
Time Time::operator+(const Time &clock)
{
	Time temp;

	temp.minute = this->minute + clock.minute;
	temp.hour = this->hour + clock.hour;
	if (temp.minute > 59)
	{
		temp.hour++;
		temp.minute -= 60;
		if (temp.hour > 23)
			temp.hour = 0;
	}
	return temp;
}

// Function:	operator==
// Description: checks if two Time objects are equal
// Return:		bool
bool Time::operator==(const Time &clock)
{
	if (this->hour == clock.hour && this->minute == clock.minute)
		return true;
	else
		return false;
}