// Program 8	(Due: April 29 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// Store the declaration for the Disk class in the file prog8disk.h. Implement all of this class' functions as inline.

// Guard preprocessor statements
#ifndef PROG8_DISK
#define PROG8_DISK
using namespace std;

// Create a class called Disk with the data members and member functions
class Disk
{
	// Private data members
private:
	int weight;
	Disk *pDnext;

	// Public member functions:
public:
	
// CONSTRUCTORS/DESTRUCTOR

	// Function:	Disk
	// Description: set all data members to values that make sense such as 0 and nullptr
	// Return:		default constructor
	Disk()
	{
		this->weight = 0;
		pDnext = nullptr;
	}

	// Function:	Disk
	// Description: accept the weight as a parameter setting data member to the given weight and pointer to nullptr
	// Return:		overloaded constructor
	Disk(int weight)
	{
		this->weight = weight;
		pDnext = nullptr;
	}

	// Function:	Disk
	// Return:		Destructor
	~Disk() { };


// MUTATOR FUNCTIONS

	// Function:	setWeight
	// Description: set the weight for the object
	// Return:		void
	void setWeight(int weight)
	{
		this->weight = weight;
	}

	// Function:	setDnext
	// Description: set the next pointer for the object
	// Return:		void
	void setDnext(Disk *pDnext)
	{
		this->pDnext = pDnext;
	}

	
// ACCESSOR FUNCTIONS

	// Function:	getWeight
	// Description: Returns the weight for the object
	// Return:		int
	int getWeight() const
	{
		return weight;
	}

	// Function:	getDnext
	// Description: Returns the next pointer for the object
	// Return:		pointer
	Disk* getDnext() const
	{
		return pDnext;
	}

};


#endif 
