// Program 8	(Due: April 29 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// Member function definitions for Tower class in a file named prog8tower.cpp
#include<iostream>
#include<string>
#include"prog8tower.h"
#include"prog8disk.h"
using namespace std;

// Declare static count variable and initialize to 1.
int Tower::count = 1;

// Function:	convert
// Description:	help to format the output of the class to console window. It will be accepting a praetor that is 1, 2, or 3 
//				and will return "A", "B", or "C" respectively.
// Return:		string
string Tower::convert(int peg) const
{
	if (peg == 1)
		return "A";
	else if (peg == 2)
		return "B";
	else
		return "C";
}

// Function:	Tower
// Description: takes number of disks in the initial tower, i.e., tower A along with pointers to linked lists. All of the parameters
//				will have default values, 0 for the first one and nullptr for all others.This constructor will create the linked
//				list represented above and set the pAHead pointer to point to the list.
// Return:		overloaded constructor
Tower::Tower(int numDisk)
{
	pAHead = nullptr;
	pBHead = nullptr;
	pCHead = nullptr;
	pATail = nullptr;
	pBTail = nullptr;
	pCTail = nullptr;

	Disk *pDnew = nullptr;
	Disk *pTemp = nullptr;

	while (numDisk != 0)
	{
		pDnew = new Disk(numDisk);
		pDnew->setDnext(nullptr);

		if (!pAHead)
			pAHead = pDnew;
		else
		{
			pTemp = pAHead;

			while (pTemp->getDnext())
				pTemp = pTemp->getDnext();
			pTemp->setDnext(pDnew);
		}
		numDisk--;
	}
	pATail = pDnew;

}

// Function:	displayTower
// Description: produce current state of the tower puzzle, i.e., nodes in each of the towers.
// Return:		void
void Tower::displayTower() const
{
	Disk* pTemp = nullptr;
	cout << "===========" << endl;
	for (int index = 1; index < 4; index++)
	{
		cout << convert(index) << ": ";
		if (index == 1)
		{
			pTemp = pAHead;
		}
		else if (index == 2)
		{
			pTemp = pBHead;
		}
		else
		{
			pTemp = pCHead;
		}
		while (pTemp)
		{
			cout << pTemp->getWeight();
			pTemp = pTemp->getDnext();
		}
		cout << endl;
	}
}

// Function:	moveDisk
// Description: accept two integer parameters. The first one will be the tower source which a disk is to be moved and 
//				the second one will be the tower destination which the disk will be moved. The source value can be 1, 2, 3 to mean tower A, B, C 
//				and the destination parameter value can also be 1,2,or 3 to mean A, B, or C tower. Depending on values of the source and destination values 
//				you will need to remove top disk node from appropriate tower linked list (A, B, or C) and ADD that node to the END of the destination linked list of disk nodes.
// Return:		void
void Tower::moveDisk(int pegBefore, int pegAfter)
{
	Disk *pTemp = nullptr;
	if (pegBefore == 1 ) 
	{
		if (pegAfter == 3)
		{
			if (!pCHead)
			{
				pCHead = pATail;
				pCTail = pATail;
			}
			else
			{
				pCTail->setDnext(pATail);
				pCTail = pATail;
			}
		}
		else if (pegAfter == 2)
		{
			if (!pBHead)
			{
				pBHead = pATail;
				pBTail = pATail;
			}
			else
			{
				pBTail->setDnext(pATail);
				pBTail = pATail;
			}
		}
		pTemp = pAHead;

		if (pAHead == pATail)
		{
			pAHead = nullptr;
			pATail = nullptr;
		}
		else
		{
			while (pTemp->getDnext() != pATail)
				pTemp = pTemp->getDnext();
			pTemp->setDnext(nullptr);
			pATail = pTemp;
		}
	}
	
	else if (pegBefore == 2)
	{
		if (pegAfter == 1)
		{
			if (!pAHead)
			{
				pAHead = pBTail;
				pATail = pBTail;
			}
			else
			{
				pATail->setDnext(pBTail);
				pATail = pBTail;
			}
		}
		else if (pegAfter == 3)
		{
			if (!pCHead)
			{
				pCHead = pBTail;
				pCTail = pBTail;
			}
			else
			{
				pCTail->setDnext(pBTail);
				pCTail = pBTail;
			}
		}
		pTemp = pBHead;

		if (pBHead == pBTail)
		{
			pBHead = nullptr;
			pBTail = nullptr;
		}
		else
		{
			while (pTemp->getDnext() != pBTail)
				pTemp = pTemp->getDnext();
			pTemp->setDnext(nullptr);
			pBTail = pTemp;
		}
	}
	
	else if (pegBefore == 3)
	{
		if (pegAfter == 2)
		{
			if (!pBHead)
			{
				pBHead = pCTail;
				pBTail = pCTail;
			}
			else
			{
				pBTail->setDnext(pCTail);
				pBTail = pCTail;
			}
		}
		else if (pegAfter == 1)
		{
			if (!pAHead)
			{
				pAHead = pCTail;
				pATail = pCTail;
			}
			else
			{
				pATail->setDnext(pCTail);
				pATail = pCTail;
			}
		}
		pTemp = pCHead;
		if (pTemp == pCTail)
		{
			pCHead = nullptr;
			pCTail = nullptr;
		}
		else
		{
			while (pTemp->getDnext() != pCTail)
				pTemp = pTemp->getDnext();
			pTemp->setDnext(nullptr);
			pCTail = pTemp;
		}
	}

	count++;
}

// Function:	moveTower
// Description: this function will be the recursive solution to the puzzle. 
//				It will have three integer parameters that will represent height of the tower, source, and destination tower.
// Return:		void
void Tower::moveTower(int numDisk, int pegBefore, int pegAfter, int pegTemp)
{
	if (numDisk > 0)
	{
		moveTower(numDisk - 1, pegBefore, pegTemp, pegAfter);
		cout << endl << count << ": " << convert(pegBefore) << " --> " << convert(pegAfter) << endl << endl;
		moveDisk(pegBefore, pegAfter);
		displayTower();
		moveTower(numDisk - 1, pegTemp, pegAfter, pegBefore);
	}
}

// Function:	Disk
// Return:		Destructor
Tower::~Tower()
{
	Disk* pTemp = pCHead;

	while (pTemp)
	{
		pCHead = pCHead->getDnext();
		delete pTemp;
		pTemp = pCHead;
	}
	pCHead = nullptr;
}