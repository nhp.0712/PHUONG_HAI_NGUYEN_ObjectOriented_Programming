// Program 8	(Due: April 29 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// Store the declaration for the Tower class in the file prog8tower.h. Implement the default constructor and the destructor as an inline functions.

// Guard preprocessor statements
#ifndef PROG8_TOWER
#define PROG8_TOWER
#include<string>
#include"prog8disk.h"
using namespace std;

// Create a class called Tower with the data members and member functions
class Tower
{
	// Private data members and functions
private:
	static int count;
	Disk* pAHead;
	Disk* pBHead;
	Disk* pCHead;
	Disk* pATail;
	Disk* pBTail;
	Disk* pCTail;
	string convert(int) const;

	// Public member functions
public:

	// Overloaded constructor
	Tower(int);

	// Destructor
	~Tower();

	// Mutator functions
	void displayTower() const;
	void moveDisk(int, int);
	void moveTower(int, int, int, int);
	
};
#endif