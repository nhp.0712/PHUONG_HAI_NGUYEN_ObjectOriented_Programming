// Program 8	(Due: April 29 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// Description:	programming OOP concepts, such as classes, encapsulation, and procedural programming concepts such as linked lists, dynamic memory allocation, pointers, recursion, and debugging.
#include<iostream>
#include<string>
#include "prog8disk.h"
#include "prog8tower.h"
using namespace std;

// Main function
int main()
{
	// Declare variables as needed
	int numDisk = 0;
	const int pegBefore = 1;
	const int pegTemp = 2;
	const int pegAfter = 3;

	// Ask user to enter the number of disks
	cout << "How many disks? ";
	cin >> numDisk;

	// Using the while loop to ask user to enter the number of disks again if it is < 1 and > 9. 
	while (numDisk < 1 || numDisk > 9)
	{
		cout << "ERROR: number of disks has to be >=1 or <= 9. Please re-enter." << endl;
		cout << "How many disks? ";
		cin >> numDisk;
	}

	// Dynamically instantiates Tower object
	Tower *TowersOfHanoi = new Tower(numDisk);

	// Call displayTower function
	TowersOfHanoi->displayTower();
	cout << endl;

	// Call moveTower function (recursive function)
	TowersOfHanoi->moveTower(numDisk, pegBefore, pegAfter, pegTemp);

	cout << endl << "S O L V E D" << endl;
	cout << "===========" << endl;

	return 0;
}