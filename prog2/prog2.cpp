// Program 2
// Description: Practice designing, implementing program solutions in C++. Debugging, file IO, data structures, UNIX.
//				This assignment is to create the solution for the following interactive program. This program will receive input from the keyboard,
//				the final photo on BGUnix will get input from a file through a special command.  
// Programmer:  Phuong Hai Nguyen
// Class:		CS 2020
// Semester:	Spring 2016


#include<iostream>
#include<string>
using namespace std;

// Declare a global constant called MAXSTUD set to 30.
const int MAXSTUD = 30;

// Define a structure called Student that contains members:
struct Student
{
	int id;
	string name;
	string zip;
	double balance;
	string lastPay;
};

// Function prototypes.
int menu();
void addStud(Student Studentlist[],int );
void updateStud(Student Studentlist[], int);
void displayStud(Student Studentlist[], int);
int findStud(Student Studentlist[], int, int);

// Main function.
int main()
{

// Declare all arrays and variables needed.
	Student Studentlist[MAXSTUD];
	int choice;
	int numStud = 0;

// Call the menu function to get the valid menu choice.
	choice = menu();

// Using while loop following the choice.
	while (choice != 5)
	{
		if (choice == 1)
		{
// Call the addStud function to add a student to the end of the list. 
			addStud(Studentlist, numStud);
			numStud++;
		}

		else if (choice == 2)
		{
// Call the updateStud function to update a student's data.
			updateStud(Studentlist, numStud);
		}

		else if (choice == 3)
		{
// Call the displayStud function to print all of the information in tabular form. 
			displayStud(Studentlist, numStud);
		}

		else if (choice == 4)
		{
// Ask user to enter student ID want to find.
			int id;
			int find = 0;
			cout << "Enter a student ID number to find: ";
			cin >> id;

// Then call the findStud function to locate a student in the array of students.
			find = findStud(Studentlist, numStud, id);

// Main function displays the result if found or not.
			if (find != -1)
			{
				cout << endl;
				cout << "Student ID number was found successfully!" << endl << endl;
				cout << "Student ID:              " << Studentlist[find].id << endl;
				cout << "Student name:            " << Studentlist[find].name << endl;
				cout << "Student zip code:        " << Studentlist[find].zip << endl;
				cout << "Student account balance: " << Studentlist[find].balance << endl;
				cout << "Date of last payment:    " << Studentlist[find].lastPay << endl << endl;
			}

			else
				cout << "Student ID number was not found!" << endl << endl;
		}
// Ask user again to make another choice.
		choice = menu();
	}

	system("pause");
	return 0;
}

// Function:	menu   
// Description: The function has no parameter list but will return a number between 1 and 5.
//				USE DATA VALIDATION in the function to make sure the value is valid. 
// Returns:		Return a number between 1 and 5 (Choice)
int menu()
{
	int choice;
// Display the menu of choices.
	cout << "          Menu of choices" << endl;
	cout << "+++++++++++++++++++++++++++++++++++++++" << endl;
	cout << "1. Enter new account information" << endl;
	cout << "2. Change account information" << endl;
	cout << "3. Display all account information" << endl;
	cout << "4. Find student" << endl;
	cout << "5. Exit the program" << endl;
// Ask user to enter the choice
	cout << "Enter your choice: ";
	cin >> choice;
// Ask user to enter the valid choice if the first choice is invalid
	if (choice != 1 || choice != 2 || choice != 3 || choice != 4 || choice != 5)
	{
		while (choice < 1 || choice > 5)
		{
			cout << "Please enter 1, 2, 3, 4 or 5." << endl;
			cout << "Enter your choice: ";
			cin >> choice;
		}
	}
// Return the choice number (between 1 and 5)
	return choice;
}

// Function:	addStud   
// Description: This function asks the user to enter data for a student 
//				and simultaneously add it to the bottom of the array.
//				This function must not accept negative values for the balance.
//				Use a data validation loop. Do not allow student id to be reused, i.e., 
//				cannot be shared between two students. Validate user id.  
// Returns:		void

void addStud(Student Studentlist[], int numStud)
{
// Check to make sure there is room in the array before calling a function. Use an if statement
	if (numStud == MAXSTUD)
		cout << "No space to enter new student." << endl << endl;
	else
	{
		cout << "Enter the student ID number: ";
		cin >> Studentlist[numStud].id;

		for (int i = 0; i < numStud; i++)
		{
// Check if id is already used. Then ask user to enter another new id, as well as other data
			if (Studentlist[numStud].id == Studentlist[i].id)
			{
				cout << "This ID number is already used. Please enter another ID." << endl;
				cout << "Enter student ID number: ";
				cin >> Studentlist[numStud].id;
			}
		}

		cout << "Enter the student name : ";
		cin.ignore();
		getline(cin, Studentlist[numStud].name);
		cout << "Enter the student zip code: ";
		cin >> Studentlist[numStud].zip;
		cout << "Enter the student account balance: ";
		cin >> Studentlist[numStud].balance;

// Check if balance value is negative. Then ask user to enter a positive value.
			while (Studentlist[numStud].balance < 0)
			{
				cout << "You cannot add negative values for the balance. Please enter a positive values for the balance." << endl;
				cout << "Enter student account balance: ";
				cin >> Studentlist[numStud].balance;
			}

		cout << "Enter the date of last payment: ";
		cin.ignore();
		getline(cin, Studentlist[numStud].lastPay);
		cout << endl;
		cout << "Student information added successfully!" << endl << endl;
	}
}

// Function:	updateStud  
// Description: This function asks the user for the student's ID. The function then calls a function called findStud
//				UpdateStud function will use the return value from findStud to update all of the student information or state that the student was not found
//				To update the data, the updateStud function will ask the user to enter new data for the student
//				Do not accept negative values for the balance. Use a data validation loop
// Returns:		void
void updateStud(Student Studentlist[], int numStud)
{
	int id;
	cout << "Enter a student ID number to update: ";
	cin >> id;
	int index = findStud(Studentlist, numStud, id);

// Ask user to update new data for update if student ID is found.
	if (index != -1)
	{
		cout << "Enter a new ID number for the student: ";
		cin >> Studentlist[index].id;
		cout << "Enter the student name : ";
		cin.ignore();
		getline(cin, Studentlist[index].name);
		cout << "Enter the student zip code: ";
		cin >> Studentlist[index].zip;
		cout << "Enter the student account balance: ";
		cin >> Studentlist[index].balance;

// Check if balance value is negative. Then ask user to enter a positive value.
		while (Studentlist[index].balance < 0)
		{
			cout << "You cannot add negative values for the balance. Please enter a positive values for the balance." << endl;
			cout << "Enter student account balance: ";
			cin >> Studentlist[index].balance;
		}

		cout << "Enter the date of last payment: ";
		cin.ignore();
		getline(cin, Studentlist[index].lastPay);
		cout << "Student information updated successfully!" << endl << endl;
	}
	else

// Display output if student ID is not found.
	{
		cout << endl;
		cout << "Student with ID number " << id << " is not found!" << endl << endl;
	}
}

// Function:	displayStud 
// Description: This function will print the data in a tabular form with headers. 
// Returns:		void
void displayStud(Student Studentlist[], int numStud)
{
// Display student list information if there is at least one student added.
	if (numStud != 0)
	{
		cout << endl;
		cout << "===================================" << endl;
		cout << "      Student list information" << endl;
		cout << "===================================" << endl;
		for (int i = 0; i < numStud; i++)
		{
			cout << "Student ID:              " << Studentlist[i].id << endl;
			cout << "Student name:            " << Studentlist[i].name << endl;
			cout << "Student zip code:        " << Studentlist[i].zip << endl;
			cout << "Student account balance: " << Studentlist[i].balance << endl;
			cout << "Date of last payment:    " << Studentlist[i].lastPay << endl << endl;
		}
	}

// Display output if there is no student added yet.
	else
		cout << "No student information to display!" << endl << endl;
}

// Function:	findStud   
// Description: This function will accept as parameters, the array, and the number of students and the id of the student to be found. 
//				The function will not display any output.
// Returns:		The function will return the subscript or -1 if not found.
int findStud(Student Studentlist[], int numStud, int id)
{
	int index = -1;
	for (int i = 0; i < numStud; i++)
	{
		if (Studentlist[i].id == id)
		{
			index = i;
			break;
		}
	}
	return index;
}
