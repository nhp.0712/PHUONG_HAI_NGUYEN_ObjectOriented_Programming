// Lab 9		(Due: April 25 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// Description: Practice writing C++ object oriented programs that enable polymorphism, use inheritance, encapsulation and virtual functions.
#include<iostream>
#include<string>
#include<fstream>
#include<iomanip>
#include"lab9manager.h"
#include"lab9employee.h"
using namespace std;

// a generic function
void PrintPay(Employee *, double);

// Main function
int main()
{
	// Create an array of SIZE 25
	const int SIZE = 25;
	Employee *LIST[SIZE];

	// Open file data
	ifstream infile;
	infile.open("lab9.txt");
	
	// Declare variables as needed
	string name;
	string first;
	string last;
	double payRate;
	bool salaried;
	int type = 0, i = 0;

	// Using if statement to check if the file is opened successfully or not.
	if (infile)
	{
		// Using while loop to read in the data from the file.
		while (!infile.eof())
		{
			infile >> type;

			// Using if/else if statement to check if the type is "1" or "2"
			if (type == 1) // Reading employees information
			{
				infile >> first;
				infile >> last;
				name = first + " " + last;
				infile >> payRate;
				LIST[i] = new Employee(name, payRate);
			}
			else if (type == 2) // // Reading managers information
			{
				infile >> first;
				infile >> last;
				name = first + " " + last;
				infile >> payRate;
				infile >> salaried;
				LIST[i] = new Manager(name, payRate, salaried);
			}
			
			i++;

		}
	}
	else
		cout << "File opened error!!!" << endl;

	cout << right << setw(30) << "Employee's salaries" << endl;
	cout << "-------------------------------------------" << endl;

	// Using a for loop to display the data for all of the data read in and stored in the array.

	for (int count = 0; count < i; count++)
	{

		// Call PrintPay function
		PrintPay(LIST[count], 40.00);
	}

	// Close file data.
	infile.close();

	return 0;
}

// Function:	PrintPay
// Description: accept as parameters pointer to an Employee object, and hours worked, and call the pay() function
// Return:		void
void PrintPay(Employee *LIST, double hours)
{
	cout << fixed << setprecision(2);
	cout << left << setw(16) << LIST->getName() << " earned " << right << setw(7) << "$" << right << setw(7) << LIST->pay(hours) << endl;
}
