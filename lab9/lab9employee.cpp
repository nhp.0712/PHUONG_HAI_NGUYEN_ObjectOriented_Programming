// Lab 9		(Due: April 25 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// All member function definitions for Employee class in a file named lab9employee.cpp
#include<iostream>
#include<string>
#include"lab9employee.h"
using namespace std;

// Function:	Employee
// Description: Accepts name and pay rate as parameters
// Return:		overloaded constructor
Employee::Employee(string name, double payRate)
{
	this->name = name;
	this->payRate = payRate;
}

// Function:	getName
// Return:		string
string Employee::getName() const
{
	return name;
}

// Function:	getPayRate
// Return:		double
double Employee::getPayRate() const
{
	return payRate;
}

// Function:	pay
// Description: return amount of pay (double) given one parameter: hours worked
// Return:		double
double Employee::pay(double hours) const
{
	return payRate * hours;
}