// Lab 9		(Due: April 25 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// All member function definitions for Manager class in a file named lab9manager.cpp
#include<iostream>
#include<string>
#include"lab9manager.h"
using namespace std;

// Function:	Employee
// Description: Accepts name, pay rate, and true/false value to designate salaried or not
// Return:		overloaded constructor
Manager::Manager(string name, double payRate, bool salaried) : Employee(name, payRate)
{
	this->salaried = salaried;
}

// Function:	getSalaried
// Return:		bool
bool Manager::getSalaried() const
{
	return salaried;
}

// Function:	pay
// Description: return the amount of pay for a manager. If a manager is salaried,
//				then payrate should be returned, otherwise pay should be computed as pay rate times hours worked.
// Return:		double
double Manager::pay(double hours) const
{
	if (!getSalaried())
		return Employee::pay(hours);
	else
		return payRate;
}

