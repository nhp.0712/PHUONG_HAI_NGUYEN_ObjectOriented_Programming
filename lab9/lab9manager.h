// Lab 9		(Due: April 25 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// Store the declaration for the Manager class in the file lab9employee.h

// Guard preprocessor statements
#ifndef LAB9_MANAGER
#define LAB9_MANAGER
#include<iostream>
#include<string>
#include"lab9employee.h"
using namespace std;

// Create a Manager class that will inherit from Employee.
class Manager : public Employee
{
	// Protected data members:
protected:
	bool salaried;

	// Public member functions:
public:

	// Overloaded constructor
	Manager(string, double, bool);

	// Accessor function
	bool getSalaried() const;
	double pay(double) const;
};

#endif