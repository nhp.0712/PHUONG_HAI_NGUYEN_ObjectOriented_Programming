// Lab 9		(Due: April 25 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// Store the declaration for the Employee class in the file lab9employee.h

// Guard preprocessor statements
#ifndef LAB9_EMPLOYEE
#define LAB9_EMPLOYEE
#include<iostream>
#include<string>
using namespace std;

// Create a base class Employee that will contain the following data members and member functions
class Employee
{
	// Protected data members:
protected:
	string name;
	double payRate;

	// Public member functions:
public:

	// Overloaded constructor
	Employee(string, double);

	// Accessor function
	string getName() const;
	double getPayRate() const;
	virtual double pay(double) const;
};

#endif 
