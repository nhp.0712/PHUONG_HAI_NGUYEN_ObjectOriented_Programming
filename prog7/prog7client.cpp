// Program 7	(Due: April 22 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// Description:	use C++ inheritance to create a base class and two derived classes and working with
//				constructors and redefining / overloading a class member function.
#include<iostream>
#include<string>
#include<iomanip>
#include<fstream>
#include"prog7ferryship.h"
#include"prog7cruiseship.h"
#include"prog7ship.h"
using namespace std;

// Main function
int main()
{
	string name, date;
	int year = 0; 
	int MaxCruiseNum = 0;
	int SignedNum = 0;
	int MaxFerryNum = 0;
	int MaxCars = 0;
	// Declare a Ship object initializing the data members with "Flying Dutchman" and "1795".
	Ship ship1("Flying Dutchman", 1795);

	// Declare a CruiseShip object with arguments for all data members using the values "Titanic", 1912, 4 / 10 / 12, 2200 and 1324.
	CruiseShip cruiseship1("Titanic", 1912, "4/10/12", 2200, 1324);

	// Declare a FerryShip object initializing the data members with "Barberi" 1981, 6,000 and 0.
	FerryShip ferryship1("Barberi", 1981, 6000, 0);

	// Call the displayShip function to display the information about each of these objects.
	ship1.displayShip();
	cruiseship1.displayShip();
	ferryship1.displayShip();

	// Define an array of pointers to Ship objects to store pointers of up to fifteen Ship, CruiseShip or FerryShip objects.
	const int INDEX = 15;
	Ship *ShipList[INDEX];

	// Open the file
	ifstream infile;
	infile.open("prog7.txt");

	// Decalre variables as needed.
	string code;
	int i = 0;

	// Using if statement to check if the file is opened successfully or not.
	if (infile)
	{
		// Using while loop to read in the data from the file.
		while (!infile.eof())
		{
			infile >> code;

			// Using if/else if statement to check if the code is "S", "C", or "F".
			if (code == "S") // If the code is "S" then the code is followed by the ship's name and year.
			{
				infile.ignore();
				getline(infile, name);
				infile >> year;
				ShipList[i] = new Ship(name, year);
			}
			else if (code == "C") // If the code is "C" then the ship�s name, year, date of departure, maximum number of passengers and number of passengers
								 //signed up for the cruise are also included in the data file.
			{
				infile.ignore();
				getline(infile, name);
				infile >> year;
				infile.ignore();
				getline(infile, date);
				infile >> MaxCruiseNum;
				infile >> SignedNum;
				ShipList[i] = new CruiseShip(name, year, date, MaxCruiseNum, SignedNum);

			}
			else if (code == "F") // If the code is "F" then the code is followed by the ship's name, year, people and car capacity.
			{
				infile.ignore();
				getline(infile, name);
				infile >> year;
				infile >> MaxFerryNum;
				infile >> MaxCars;
				ShipList[i] = new FerryShip(name, year, MaxFerryNum, MaxCars);
			}
			i++;
		}

		// Using a for loop to display the data for all of the data read in and stored in the array.
		for (int count = 0; count < i; count++)
			ShipList[count]->displayShip();
		cout << "----------------------------------------" << endl;
	}
	else
		cout << "File opened error!!!" << endl;

	infile.close();

	return 0;
}