// Program 7	(Due: April 22 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// All member function definitions for CruiseShip class in a file named prog7cruiseship.cpp
#include<iostream>
#include<string>
#include<iomanip>
#include"prog7cruiseship.h"
using namespace std;

// Function:	CruiseShip
// Description: invokes the Ship default constructor to initialize the inherited data members and sets the CruiseShip
//				fields to zero or the empty string.
// Return:		default constructor
CruiseShip::CruiseShip() : Ship()
{
	date = "";
	MaxCruiseNum = 0;
	SignedNum = 0;
}

// Function:	CruiseShip
// Description: Accepts a cruise ship's name, year, departure date, maximum number of passengers and number signed up.
//				Pass the first two arguments to the Ship constructor and use the last three to set the CruiseShip's data members.
// Return:		overloaded constructor
CruiseShip::CruiseShip(string name, int year, string date, int MaxCruiseNum, int SignedNum) : Ship(name, year)
{
	this->date = date;
	this->MaxCruiseNum = MaxCruiseNum;
	this->SignedNum = SignedNum;
}

// Function:	PercentCal
// Description: calculate the percentage filled for the cruise (number signed up / maximum number of passengers)
// Return:		double
double CruiseShip::PercentCal() const
{
	return ((double) SignedNum / MaxCruiseNum * 100);
}

// Function:	displayShip
// Description: overrides the displayShip function in the base class. It should display the ship's name, year, departure date,
//				maximum number of passengers, passengers signed up for the cruise and the percentage filled for the cruise.
// Return:		void
void CruiseShip::displayShip() const
{
	Ship::displayShip();
	cout << "CRUISE SHIP" << endl;
	cout << left << setw(25) << "Departing: " << date << endl;
	cout << left << setw(25) << "Max # of passengers: " << MaxCruiseNum << endl;
	cout << left << setw(25) << "# Passengers sailing: " << SignedNum << endl;
	cout << setprecision(2) << fixed;
	cout << left << setw(25) << "Percent full: " << PercentCal() << "%" << endl;
}