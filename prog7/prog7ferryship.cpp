// Program 7	(Due: April 22 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// All member function definitions for FerryShip class in a file named prog7ferryship.cpp
#include<iostream>
#include<string>
#include<iomanip>
#include"prog7ferryship.h"
using namespace std;

// Function:	FerryShip
// Description: invokes the Ship default constructor to initialize the inherited data members and sets number of
//				passengers and cars to zero.
// Return:		default constructor
FerryShip::FerryShip() : Ship()
{
	MaxFerryNum = 0;
	MaxCars = 0;
}

// Function:	FerryShip
// Description: Accepts a ferry ship's name, year, people and car capacity. Pass the first two arguments to the Ship
//				constructor and use the last two to set the FerryShip's data members
// Return:		overloaded constructor
FerryShip::FerryShip(string name, int year, int MaxFerryNum, int MaxCars) : Ship(name, year)
{
	this->MaxFerryNum = MaxFerryNum;
	this->MaxCars = MaxCars;
}

// Function:	displayShip
// Description: overrides the displayShip function in the base class. It should first call the base class's function (to
//				display the ship's name and year) and then display the ship's data members.
// Return:		void
void FerryShip::displayShip() const
{
	Ship::displayShip();
	cout << "FERRY SHIP" << endl;
	cout << left << setw(25) << "Max # of passengers: " << MaxFerryNum << endl;
	cout << left << setw(25) << "Max # of cars: " << MaxCars << endl;
}