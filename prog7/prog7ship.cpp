// Program 7	(Due: April 22 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// All member function definitions for Ship class in a file named prog7ship.cpp
#include<iostream>
#include<string>
#include<iomanip>
#include"prog7ship.h"
using namespace std;

// Function:	Ship
// Description: Sets the name to the empty string and the year to zero
// Return:		default constructor
Ship::Ship()
{
	name = "";
	year = 0;
}

// Function:	Ship
// Description: Accepts values for the name and year and assigns them to the data members
// Return:		overloaded constructor
Ship::Ship(string name, int year)
{
	this->name = name;
	this->year = year;
}

// Function: setShip
// Description: Set values for the name and year and assigns them to the data members
// Return: void
void Ship::setShip(string name, int year)
{
	this->name = name;
	this->year = year;
}

// Function: getName
// Return: string
string Ship::getName() const
{
	return name;
}

// Function: getYear
// Return: int
int Ship::getYear() const
{
	return year;
}

// Function:	displayShip
// Description: Display the ship's name and the year it was built
// Return:		void
void Ship::displayShip() const
{
	cout << "----------------------------------------" << endl;
	cout << left << setw(25) << "Ship: " << name << endl;
	cout << left << setw(25) << "Year: " << year << endl << endl;

}