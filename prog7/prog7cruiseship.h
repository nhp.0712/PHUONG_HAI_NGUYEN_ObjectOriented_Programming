// Program 7	(Due: April 22 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// Design a CruiseShip class that is derived from the Ship class.
// Store the declaration for this class in a file called prog7cruiseship.h

// Guard preprocessor statements
#ifndef PROG7_CRUISESHIP
#define PROG7_CRUISESHIP
#include<iostream>
#include<string>
#include<iomanip>
#include"prog7ship.h"
using namespace std;

// Create a derived class called CruiseShip with additional information about Ship with the data members and member functions
class CruiseShip : public Ship
{
	// Private data members:
private:
	string date;
	int MaxCruiseNum;
	int SignedNum;

	// Public member functions:
public:

	// Default constructor
	CruiseShip();

	// Overloaded constructor
	CruiseShip(string, int, string, int, int);

	// Accessor functions
	double PercentCal() const;
	void displayShip() const;

};

#endif