// Program 7	(Due: April 22 2016)
// Author:		Phuong Hai Nguyen
// Class:		CS2020 (Section: 3 - 10:30)
// Design a FerryShip class that is derived from the Ship class.
// Store the declaration for this class in a file called prog7ferrship.h

// Guard preprocessor statements
#ifndef PROG7_FERRYSHIP
#define PROG7_FERRYSHIP
#include<iostream>
#include<string>
#include<iomanip>
#include"prog7ship.h"
using namespace std;

// Create a derived class called FerryShip with additional information about Ship with the data members and member functions

class FerryShip : public Ship
{
	// Private data members:
private:
	int MaxFerryNum;
	int MaxCars;

	// Public member functions:
public:

	// Default constructor
	FerryShip();

	// Overloaded constructor
	FerryShip(string, int, int, int);

	// Accessor function
	void displayShip() const;
};

#endif