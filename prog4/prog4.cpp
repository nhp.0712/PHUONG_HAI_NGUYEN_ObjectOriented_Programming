// Program 4
// Description: Practice designing and implementing programs in C++. Debugging, file IO, data structures, linked lists, pointers, UNIX 
//				Your task is to complete program that maintains a linked list of students. You will use a struct called Student that contains a name,
//				id, status and gpa as members. The program will read command codes and data from a file to process the list. Here is a list of the
//				functionality your program will implement.
//				(add one student to the list, remove one student from the list, a listing of the students, 
//				a listing of all of the student for a given classification(Senr, JunR, Soph, Fresh), 
//				a listing of all of the deanslist students(above 3.5 gpa), destroy the list of students upon program termination.)
// Programmer:  Phuong Hai Nguyen
// Class:		CS 2020
// Semester:	Spring 2016
#include<iostream>
#include<fstream>
#include<iomanip>
#include<string>

using namespace std;

// Define a Student structure that contains : name, id, status, gpa
// Then declare a Next pointer in structure.
struct Student
{
	string Name;
	int Id;
	string Status;
	double Gpa;
	Student * nextStudent;
};

// Declare two enumerated types: one for class rank and Selection of program operations.
// Then set enumerators appropriately so that Fresh =1, Soph = 2, etc. and program operations correspond to the data file numerical values.
enum ClassRank { Fresh = 1, Soph, Junr, Senr };
enum Selection { QUIT, ADD, REMOVE, PRINTLIST, CLASSLIST, DEANSLIST };

// Function prototypes:
Student* CreateStudent(ifstream &);
void AddStudent(Student* &, Student*);
void RemoveStudent(Student* &, ifstream &);
void PrintList(Student*);
void ClassList(Student*, ifstream &);
void DeansList(Student*);
void ClearList(Student* &);
string getClassName(int);

// Main function
int main()
{
	// Declare all pointers and variables needed, and allocate space using pointer variables for Student structure.
	Student * FirstStud;
	FirstStud = nullptr;
	Student * NewStud;
	NewStud = nullptr;
	int choice = -1;

	// Declare the file input object and open the file.
	ifstream infile;
	infile.open("prog4.txt");

	// Returns a pointer to that memory via the return statement to read the next student record from the data file and assign the values to newly allocated memory
	FirstStud = CreateStudent(infile);

	// Ask user to select from the following menu of options:
	while (choice != QUIT)
	{
		cout << "Command Code" << endl;
		cout << "----------------" << endl;
		cout << "1 Add Student" << endl;
		cout << "2 Remove Student" << endl;
		cout << "3 Print List" << endl;
		cout << "4 Class List" << endl;
		cout << "5 Deans List" << endl;
		cout << "0 Quit" << endl;
		cin >> choice;
		cout << endl;

		switch (choice)
		{
		case ADD:
			// Call AddStudent function to add the student's data to the list.
			AddStudent(FirstStud, NewStud);
			break;
		case REMOVE:
			// Call RemoveStudent function to remove the student's data from the list
			RemoveStudent(FirstStud, infile);
			break;
		case PRINTLIST:
			// Call PrintList function to print all of the data about each student in tabular form.
			PrintList(FirstStud);
			break;
		case CLASSLIST:
			// Call ClassList function to print a list of all of the students in that class in tabular form
			ClassList(FirstStud, infile);
			break;
		case DEANSLIST:
			// Call DeansList function to print all of the Dean�s List student in tabular form. (gpa of 3.5 or better)
			DeansList(FirstStud);
			break;
		}
	}
	cout << endl;

	// Call ClearList function to remove all of the students from the list before the program terminates and print the name of the student right before the student is removed
	ClearList(FirstStud);

	infile.close();

	return 0;
}

// Function:	CreateStudent   
// Description: Reads the next student record from the data file and assigns the values to newly allocated (Student structure) memory. 
//				Returns a pointer to that memory via the return statement. File object variable is passed to this function. Upon return, in main, capture the returned address into NewStud pointer
// Returns:		Student pointer
Student * CreateStudent(ifstream &infile)
{
	// Declare all pointers and variables needed
	Student * NewStud;
	NewStud = nullptr;
	Student *pNew;
	Student *ptr;

	// Use if/else statement to check if the file opened successfully or not.
	if (infile)
	{
		// When the file opened successfully, using while loop to read in all the data and store into the list
		while (!infile.eof())
		{
			string line;
			int check;

			infile >> check;

			if (check == 1)
			{
				pNew = new Student;
				pNew->nextStudent = nullptr;
				// Read the data and store to variables of Student structure by using pointer
				getline(infile, line);
				
				if (line.length() > 5)
				{
					pNew->Name = line;
					infile >> pNew->Id;
					infile >> pNew->Status;
					infile >> pNew->Gpa;

					// Link the Student list
					if (NewStud == nullptr)
						NewStud = pNew;
					else
					{
						ptr = NewStud;
						while (ptr->nextStudent != nullptr)
							ptr = ptr->nextStudent;

						ptr->nextStudent = pNew;
					}
				}
			}
		}
		return NewStud;
	}
	else
	{
		cout << "File opened unsuccessfully!" << endl;
		return nullptr;
	}
}

// Function:	AddStudent
// Description: Accepts the FirstStud pointer and a pointer to the NewStud as arguments and adds the NewStud to the front of the List. Print out a
//				message indicating that the student was added. The FirstStud pointer must be passed byref.
// Returns:		void
void AddStudent(Student* &FirstStud, Student* NewStud)
{
	NewStud = new Student;
	// Ask user to enter the student information and store it.
	cout << "Enter student name: ";
	cin.ignore();
	getline(cin, NewStud->Name);
	cout << endl << "Enter ID: ";
	cin >> NewStud->Id;
	cout << endl << "Enter Status (Please enter: 'Fresh, Soph, Junr, or Senr' for 'Freshman, Sophomore, Junior, or Senior': ";
	cin >> NewStud->Status;
	cout << endl << "Enter GPA: ";
	cin >> NewStud->Gpa;

	NewStud->nextStudent = FirstStud;
	FirstStud = NewStud;

	cout << endl << "The student was added successfully! " << endl;
}

// Function:	RemoveStudent
// Description: Accept the FirstStud pointer. This function should read the ID from the data file and remove that student from the list and print a
//				message indicating the results of the removal. The student may not be on the list.
// Returns:		void
void RemoveStudent(Student* &FirstStud, ifstream &infile)
{
	int IdNum = 0, check = 0;
	// Ask user to enter the student's ID to remove from the list.
	cout << "Enter the student's ID to remove from the list: ";
	cin >> IdNum;

	Student *pTemp = nullptr;
	Student *pAfter = nullptr;

	// Find and remove the student if the student's data is in the first node.
	if (FirstStud->Id == IdNum)
	{
		pAfter = FirstStud;
		FirstStud = pAfter->nextStudent;
		delete pAfter;

		cout << endl << "The student was removed successfully!" << endl;
		return;
	}

	pTemp = FirstStud;
	pAfter = FirstStud->nextStudent;

	// Find and remove the student by traversing the list.
	while (pAfter)
	{
		if (pAfter->Id == IdNum)
		{
			check = 1;
			pTemp->nextStudent = pAfter->nextStudent;
			delete pAfter;

			cout << endl << "The student was removed successfully!" << endl;
			break;
		}

		pTemp = pAfter;
		pAfter = pAfter->nextStudent;
	}

	// Display output if student's id was not found.
	if (check == 0)
		cout << endl << "The student's ID was not found!!!" << endl;

}

// Function:	PrintList
// Description: Accept the FirstStud pointer. Print all of the data about each student in tabular form.
// Returns:		void
void PrintList(Student* FirstStud)
{
	Student * ptr;
	ptr = FirstStud;
	int num = 1, findClass = 0;

	cout << "====== Print List ======" << endl;

	// Using while loop to traverse the list to print all of the data about each student in tabular form.
	while (ptr)
	{
		cout << right << setw(3) << num++ << setw(19) << ptr->Name << setw(5) << ptr->Id << setw(5) << ptr->Gpa;

		if (ptr->Status == "Fresh")
			findClass = Fresh;
		else if (ptr->Status == "Soph")
			findClass = Soph;
		else if (ptr->Status == "Junr")
			findClass = Junr;
		else
			findClass = Senr;

		cout << setw(4) << findClass << endl;
		ptr = ptr->nextStudent;
	}
	cout << endl;
}

// Function:	ClassList
// Description: Accept the FirstStud pointer. This function should read in a numerical value for the classification from the data file and print a list of
//				all of the students in that class in tabular form. (Senr = 4, JunR = 3, Soph = 2, Fresh = 1). If there are no students in that class, print a
//				message indicating. The header will always be printed.
// Returns:		void
void ClassList(Student* FirstStud, ifstream &infile)
{
	Student * ptr;
	ptr = FirstStud;
	int num = 1, choice = 0;
	bool flag = false;

	// Ask user to enter the class rank.
	cout << "Please enter a number between 1 to 4 (1: Freshman, 2: Sophomore, 3: Junior, 4: Senior): ";
	cin >> choice;

	// Using do-while loop to traverse the list to print a list of all of the students in that class in tabular form.
	do
	{
		if (choice > 0 && choice < 5)
		{
			flag = true;
			string ClassName = getClassName(choice);



			while (ptr)
			{
				if (ptr->Status == ClassName)
					cout << right << setw(3) << num++ << setw(20) << ptr->Name << setw(5) << ptr->Id << setw(5) << choice << endl;

				ptr = ptr->nextStudent;

			}
		}
		else
		{
			while (choice < 1 || choice >4)
			{
				// Ask user to enter the class rank again if the last choice was incorrect.
				cout << "Incorrect Choice!!!" << endl;
				cout << "Please enter a number between 1 to 4 (1: Freshman, 2: Sophomore, 3: Junior, 4: Senior): ";
				cin >> choice;
			}
		}
	} while (!flag);
	cout << endl;
}

// Function:	DeansList
// Description: Accept the FirstStud pointer. Print all of the Dean�s List student in tabular form. (gpa of 3.5 or better)
// Returns:		void
void DeansList(Student* FirstStud)
{
	Student * ptr;
	ptr = FirstStud;
	int num = 1;

	cout << "====== Deans List (GPA >= 3.5) ======" << endl;

	// Using while loop to traverse the list to print all of the Dean�s List student in tabular form.
	while (ptr)
	{
		if (ptr->Gpa >= 3.5)
			cout << right << setw(3) << num++ << setw(20) << ptr->Name << setw(5) << ptr->Id << setw(5) << ptr->Gpa << endl;

		ptr = ptr->nextStudent;

	}
	cout << endl;
}

// Function:	ClearList
// Description: Accept the FirstStud pointer. This function should remove all of the students from the list before the program terminates.
//				The function will print the name of the student right before the student is removed.
// Returns:		void
void ClearList(Student* &FirstStud)
{
	Student *ptr = FirstStud;

	// Using while loop to traverse to clear the list
	while (ptr)
	{
		cout << "Remove student: " << ptr->Name << endl;
		FirstStud = FirstStud->nextStudent;
		delete ptr;
		ptr = FirstStud;
	}
	FirstStud = nullptr;
}

// Function:	getClassName
// Description: Receive class rank in numerical form and return �Freshman�, �Sophomore�, etc. via the return type.
//				This function will be called from ClassList function so that an appropriate heading can be displayed above class listing.
// Returns:		string
string getClassName(int choice)
{
	string ClassName;

	if (choice == 1)
	{
		ClassName = "Fresh";
		cout << "====== Freshman List ======" << endl;
	}
	else if (choice == 2)
	{
		ClassName = "Soph";
		cout << "====== Sophomore List ======" << endl;
	}
	else if (choice == 3)
	{
		ClassName = "Junr";
		cout << "====== Junior List ======" << endl;
	}
	else
	{
		ClassName = "Senr";
		cout << "====== Senior List ======" << endl;
	}

	return ClassName;

}