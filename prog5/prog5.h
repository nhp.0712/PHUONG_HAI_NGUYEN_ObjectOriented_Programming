// Guard preprocessor statements
#ifndef PROG5_H
#define PROG5_H

// Temperature class definition includes the private data members and setter and getter function prototypes.
class Temperature
{

	// Private data members
private:
	int temperature;

	// Public member functions
public:
	// Declare a default constructor that sets the temperature to zero and defined as in-line functions.
	Temperature() { temperature = 0; }

	// Declare a constructor that accepts a value for the temperature and defined as in-line functions.
	Temperature(int temp) { temperature = temp; }


	// Mutator functions:
	// Function: setTemp 
	// Return: void
	void setTemp(int temp) { temperature = temp; } // Defined as in - line functions.

	// Accessor functions:
	// Function: getTemp 
	// Return: int
	int getTemp() const { return temperature; } // Defined as in - line functions.

	// Defined as out - of - line functions.
	bool isOxygenFreezing() const;
	bool isOxygenBoiling() const;
	bool isEthylFreezing() const;
	bool isEthylBoiling() const;
	bool isWaterFreezing() const;
	bool isWaterBoiling() const;
	bool isNitrogenFreezing() const;
	bool isNitrogenBoiling() const;
};

#endif

