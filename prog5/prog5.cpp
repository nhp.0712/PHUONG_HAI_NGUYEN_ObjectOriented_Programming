// Description: the function definitions (i.e., implementations) for the out-of-line functions.
#include<iostream>
#include "prog5.h"
using namespace std;

// Function:	isOxygenFreezing   
// Description: This function should return the bool value true if the temperature stored in the temperature field
//				is at or below the freezing point of oxygen. Otherwise, the function should return false.
// Returns:		bool
bool Temperature::isOxygenFreezing() const
{
	if (temperature <= -362)
		return true;
	else
		return false;
}

// Function:	isOxygenBoiling   
// Description: This function should return the bool value true if the temperature stored in the temperature field
//				is at or above the boiling point of oxygen. Otherwise, the function should return false.
// Returns:		bool
bool Temperature::isOxygenBoiling() const
{
	if (temperature >= -300)
		return true;
	else
		return false;
}

// Function:	isEthylFreezing   
// Description: This function should return the bool value true if the temperature stored in the temperature field
//				is at or below the freezing point of ethy alcohol. Otherwise, the function should return false.
// Returns:		bool
bool Temperature::isEthylFreezing() const
{
	if (temperature <= -173)
		return true;
	else
		return false;
}

// Function:	isOxygenBoiling   
// Description: This function should return the bool value true if the temperature stored in the temperature field
//				is at or above the boiling point of ethyl alcohol. Otherwise, the function should return false.
// Returns:		bool
bool Temperature::isEthylBoiling() const
{
	if (temperature >= 173)
		return true;
	else
		return false;
}

// Function:	isWaterFreezing   
// Description: This function should return the bool value true if the temperature stored in the temperature field
//				is at or below the freezing point of water. Otherwise, the function should return false.
// Returns:		bool
bool Temperature::isWaterFreezing() const
{
	if (temperature <= 32)
		return true;
	else
		return false;
}

// Function:	isWaterBoiling   
// Description: This function should return the bool value true if the temperature stored in the temperature field
//				is at or above the boiling point of water. Otherwise, the function should return false.
// Returns:		bool
bool Temperature::isWaterBoiling() const
{
	if (temperature >= 212)
		return true;
	else
		return false;
}

// Function:	isNitrogenFreezing   
// Description: This function should return the bool value true if the temperature stored in the temperature field
//				is at or below the freezing point of nitrogen. Otherwise, the function should return false.
// Returns:		bool
bool Temperature::isNitrogenFreezing() const
{
	if (temperature <= -346)
		return true;
	else
		return false;
}

// Function:	isNitrogenBoiling   
// Description: This function should return the bool value true if the temperature stored in the temperature field
//				is at or above the boiling point of nitrogen. Otherwise, the function should return false.
// Returns:		bool
bool Temperature::isNitrogenBoiling() const
{
	if (temperature >= -320)
		return true;
	else
		return false;
}