// Program 5
// Description: Practice designing and implementing programs in C++. Debugging, classes.
//				The program should ask the user to enter a temperature and then display a list of the substances 
//				that will freeze at that temperature and those that will boil at that temperature.
// Programmer:  Phuong Hai Nguyen
// Class:		CS 2020
// Semester:	Spring 2016
#include<iostream>
#include "prog5.h"
using namespace std;

// Main function
int main()
{
	int temp;

	// Define (instantiate) a Temperature object called Substance.
	Temperature Substance;

	// Ask the user to enter a temperature
	cout << "This program will tell you what substances freeze" << endl << "or boil at a particular temperature." << endl << endl;
	cout << "Please enter a temperature: ";
	cin >> temp;

	// Store the value in the Substance's private data members.
	Substance.setTemp(temp);

	// Display a list of the substances that will freeze at that temperature and those that will boil at that temperature.
	cout << "==================================================================" << endl;
	cout << "Substances that freeze at   " << temp << ":    ";

	// Using if statement to return the bool value if the temperature stored in the temperature field is 
	// at or below the freezing point of each substance.
	if (Substance.isOxygenFreezing())
		cout << "Oxygen ";
	if (Substance.isEthylFreezing())
		cout << "Ethyl Alcohol ";
	if (Substance.isWaterFreezing())
		cout << "Water ";
	if (Substance.isNitrogenFreezing())
		cout << "Nitrogen ";

	cout << endl << "Substances that boil at     " << temp << ":    ";

	// Using if statement to return the bool value if the temperature stored in the temperature field is 
	// at or above the boiling point of each substance.
	if (Substance.isOxygenBoiling())
		cout << "Oxygen ";
	if (Substance.isEthylBoiling())
		cout << "Ethyl Alcohol ";
	if (Substance.isWaterBoiling())
		cout << "Water ";
	if (Substance.isNitrogenBoiling())
		cout << "Nitrogen ";

	cout << endl;
	return 0;
}