// Program 3
// Description: Practice designing and implementing programs in C++. Debugging, file IO, data structures, pointers, BGUnix.
//				Write a program to read in and process sales data for all of the divisions in a company.
//				This program will give practice with file input and pointers to structs.
// Programmer:  Phuong Hai Nguyen
// Class:		CS 2020
// Semester:	Spring 2016

#include<iostream>
#include<fstream>
#include<string>
#include<iomanip>

using namespace std;

// Define a Corporation structure that contains the data for the corporation including the total sales for each quarter and the number of divisions in the corporation. 
struct Corporation
{
	int totalQ1 = 0, totalQ2 = 0, totalQ3 = 0, totalQ4 = 0, NumofDiv = 0;
};

// Define a Division structure that contains the data for a company division including its name and the four quarterly sales amounts (string and integer). 
struct Division
{
	string name;
	int Q1 = 0, Q2 = 0, Q3 = 0, Q4 = 0;
};

// Function prototypes.
void getDivision(Division *, fstream &);
void printDivision(Division *);
void addDivision(Division *, Corporation *);
void printCorpSummary(Corporation *);

// Main function.
int main()
{

// Declare all pointers and variables needed.
	struct Corporation CorData;
	struct Division DivData;

// Allocate space using pointer variables for a Corporation structure and a Division structure.
	Corporation *ptrCor = nullptr;
	Division *ptrDiv = nullptr;
	ptrCor = new Corporation;
	ptrDiv = new Division;

// Declare the file input object and open the file.
	fstream infile;
	infile.open("prog3.txt");

// Use if/else statement to check if the file opened successfully or not. And displaying the output.
	if (infile)

// Displaying the formatting of output.
	{
		cout << "---------------------------------------------------------------------" << endl;
		cout << "                 Status Corporation Sales Report*" << endl;
		cout << "                        Phuong Hai Nguyen" << endl;
		cout << "---------------------------------------------------------------------" << endl;
		cout << right << setw(20) << "Division" << setw(12) << "Q1" << setw(12) << "Q2" << setw(12) << "Q3" << setw(12) << "Q4" << endl;

		// When the file opened successfully, using while loop to read in the sales data for each division from the prog3.txt file and create a report.
		// Call three functions each time through the while loop. 
		while (!infile.eof())
		{

			// Call a function named getDivision to read in the data for one division.
			getDivision(ptrDiv, infile);

			// Call a function named printDivision to write a line of output to display the division name, four quarterly sales amounts and the total sales for the year for that division.
			printDivision(ptrDiv);

			// Call a function named addDivision to add this division's sales amounts to the corporate sales amounts. 
			addDivision(ptrDiv, ptrCor);
		}

		cout << "---------------------------------------------------------------------" << endl;

		// Call a final function named printCorpSummary to print the total corporate sales for each quarter, the total sales for all quarters for the year, 
		// the average quarterly sales for each quarter and the highest and lowest quarters for the corporation.
		printCorpSummary(ptrCor);
	}

	else

		// Display error message if file opened unsuccessfully.
		cout << "File opened error!!!" << endl;

// Deallocate the space you allocated for the structures.
	delete ptrCor;
	delete ptrDiv;

	return 0;
}

// Function:	getDivision   
// Description: This funtion reads in the data for one division. Pass the Division pointer and the input file object to this function.
//				It will read in data for one division and store the data in the Division struct using the pointer variable. 
// Returns:		void
void getDivision(Division *ptrDiv, fstream &infile)
{

// Read in data for one division and store the data in the Division struct using the pointer variable.
	getline(infile, ptrDiv->name);
	infile >> ptrDiv->Q1;
	infile >> ptrDiv->Q2;
	infile >> ptrDiv->Q3;
	infile >> ptrDiv->Q4;
	infile.ignore();
}


// Function:	printDivision   
// Description: This function writes a line of output to display the division name, four quarterly sales amounts and the total sales for the year for that division.
//				Pass the pointer to the Division structure to this function
// Returns:		void
void printDivision(Division *ptrDiv)
{

//  Produce output on the screen showing the amounts for each division as well as the summary data shown in the sample output.    
	cout << right << setw(20) << ptrDiv->name << setw(12) << ptrDiv->Q1 << setw(12) << ptrDiv->Q2 << setw(12) << ptrDiv->Q3 << setw(12) << ptrDiv->Q4 << endl;
}


// Function:	addDivision   
// Description: This function adds this division's sales amounts to the corporate sales amounts. Pass the pointer of both the Corporation and Division structures to this function.
//				The "Number of Divisions" data member will be incremented each time this function is called.
// Returns:		void
void addDivision(Division *ptrDiv, Corporation *ptrCor)
{

// Calculating to add the division's sales amounts to the corporate sales amounts.
	ptrCor->totalQ1 += ptrDiv->Q1;
	ptrCor->totalQ2 += ptrDiv->Q2;
	ptrCor->totalQ3 += ptrDiv->Q3;
	ptrCor->totalQ4 += ptrDiv->Q4;
	ptrCor->NumofDiv++;

}


// Function:	printCorpSummary   
// Description: This function prints the total corporate sales for each quarter, the total sales for all quarters for the year, the average quarterly sales for each quarter 
//				and the highest and lowest quarters for the corporation. Pass the pointer to the Corporation structure to this function. 
// Returns:		void
void printCorpSummary(Corporation *ptrCor)
{
	int totalSales = 0;

// Prints the total corporate sales for each quarter.
	cout << right << setw(20) << "Corp Totals:" << setw(12) << ptrCor->totalQ1 << setw(12) << ptrCor->totalQ2 << setw(12) << ptrCor->totalQ3 << setw(12) << ptrCor->totalQ4 << endl;
	cout << right << setw(20) << "Avg Qtr Sales:" << setw(12) << (ptrCor->totalQ1) / (ptrCor->NumofDiv) << setw(12) << (ptrCor->totalQ2) / (ptrCor->NumofDiv) << setw(12) << (ptrCor->totalQ3) / (ptrCor->NumofDiv) << setw(12) << (ptrCor->totalQ4) / (ptrCor->NumofDiv) << endl;

// Calculating and print the total sales for all quarters for the year.
	totalSales = ptrCor->totalQ1 + ptrCor->totalQ2 + ptrCor->totalQ3 + ptrCor->totalQ4;

	cout << endl;
	cout << right << setw(20) << "Total Sales: " << totalSales << endl;

// Declare an integer array to hold total corporate sales for each quarter. Then declare variables to find the highest and the lowest quarters for the corporation by using for loop.
	int ArrayCor[4] = { ptrCor->totalQ1, ptrCor->totalQ2, ptrCor->totalQ3, ptrCor->totalQ4 };
	int highest = ArrayCor[0], lowest = ArrayCor[0];

	for (int i = 1; i < 4; i++)
	{
		if (highest < ArrayCor[i])
			highest = ArrayCor[i];
		if (lowest > ArrayCor[i])
			lowest = ArrayCor[i];
	}
	
// Print the highest and the lowest quarters for the corporation.
	cout << right << setw(20) << "Corp High Qtr: " << highest << endl;
	cout << right << setw(20) << "Corp Low Qtr: " << lowest << setw(40) << "* $ in thousands" << endl;

}